package com.example.robbe.commonpurse.SQLite;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.robbe.commonpurse.Constants;

public class DatabaseContentProvider extends ContentProvider {

    private static final int PAYMENT = 100;
    private static final int PAYMENT_ID = 101;
    private static final int PAYMENT_FIREBASE_ID = 102;

    private static final int USER = 200;
    private static final int USER_ID = 201;
    private static final int USER_FIREBASE_ID = 202;

    private static final int EXCHANGE_RATE = 300;
    private static final int EXCHANGE_RATE_ID = 301;

    private static final int DATABASES = 400;
    private static final int DATABASES_ID = 401;
    private static final int DATABASES_FIREBASE_ID = 402;

    private PaymentDatabaseHelper paymentDatabaseHelper = null;
    private UserDatabaseHelper userDatabaseHelper = null;
    private ExchangeRateDatabaseHelper exchangeRateDatabaseHelper = null;
    private DatabasesDatabaseHelper databasesDatabaseHelper = null;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_PAYMENT, PAYMENT);
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_PAYMENT + "/#", PAYMENT_ID);
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_PAYMENT + "/*", PAYMENT_FIREBASE_ID);

        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_USER, USER);
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_USER + "/#", USER_ID);
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_USER + "/*", USER_FIREBASE_ID);

        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_EXCHANGE_RATE, EXCHANGE_RATE);
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_EXCHANGE_RATE + "/#", EXCHANGE_RATE_ID);

        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_DATABASES, DATABASES);
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_DATABASES + "/#", DATABASES_ID);
        uriMatcher.addURI(Constants.CONTENT_AUTHOROTY, Constants.URI_DATABASES + "/?", DATABASES_FIREBASE_ID);
    }


    @Override
    public boolean onCreate() {
        createLog("Database Content Provider has been called");
        SharedPreferences prefs = getContext().getSharedPreferences(Constants.SAVED_DATABASE, Context.MODE_PRIVATE);
        String database = prefs.getString(Constants.SAVED_DATABASE_ID, null);

        // Creating the unique database in the system.
        paymentDatabaseHelper = new PaymentDatabaseHelper(getContext(),
                Constants.PaymentEntry.TABLE_NAME + database);
        userDatabaseHelper = new UserDatabaseHelper(getContext(),
                Constants.UserEntry.TABLE_NAME + database);
        exchangeRateDatabaseHelper = new ExchangeRateDatabaseHelper(getContext(),
                Constants.ExchangeRateEntry.TABLE_NAME + database);
        databasesDatabaseHelper = new DatabasesDatabaseHelper(getContext(),
                Constants.DatabaseEntry.TABLE_NAME + database);

        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        int match = uriMatcher.match(uri);

        Cursor cursor;
        switch (match) {
            case PAYMENT:
                SQLiteDatabase db = paymentDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case PAYMENT_ID:
                db = paymentDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        Constants.PaymentEntry.Col0_ID + "=?",
                        new String[]{String.valueOf(ContentUris.parseId(uri))},
                        null,
                        null,
                        sortOrder);
                break;
            case PAYMENT_FIREBASE_ID:
                db = paymentDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        Constants.PaymentEntry.COL1_FIREBASEID + "=?",
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case USER:
                db = userDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case USER_ID:
                db = userDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        Constants.UserEntry.Col0_id + "=?",
                        new String[]{String.valueOf(ContentUris.parseId(uri))},
                        null,
                        null,
                        sortOrder);
                break;
            case USER_FIREBASE_ID:
                db = userDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        Constants.UserEntry.COL1_FIREBASEID + "=?",
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case EXCHANGE_RATE:
                db = exchangeRateDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case EXCHANGE_RATE_ID:
                db = exchangeRateDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        Constants.ExchangeRateEntry.COL0_ID + " =?",
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case DATABASES:
                db = databasesDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case DATABASES_ID:
                db = databasesDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        Constants.DatabaseEntry.COL0_ID + " =?",
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case DATABASES_FIREBASE_ID:
                db = databasesDatabaseHelper.getWritableDatabase();
                cursor = db.query(getTableName(uri),
                        projection,
                        Constants.DatabaseEntry.COL1_FIREBASEID + " =?",
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query, unknown URI");
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        int match = uriMatcher.match(uri);
        Uri insertedUri = null;

        switch (match) {
            case PAYMENT:
                insertedUri =  insertPayment(uri, values);
                break;
            case USER:
                insertedUri =  insertUser(uri, values);
                break;
            case EXCHANGE_RATE:
                insertedUri =  insertExchangeRate(uri, values);
                break;
            default:
                createLog("Uri not supported");
        }

        if (insertedUri != null) {
            getContext().getContentResolver().notifyChange(insertedUri, null);
        }
        return insertedUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int match = uriMatcher.match(uri);

        switch (match) {
            case PAYMENT:
                return deletePayment(uri, null, null);
            case PAYMENT_FIREBASE_ID:
                selection = Constants.PaymentEntry.COL1_FIREBASEID + " =?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                return deletePayment(uri, selection, selectionArgs);
            case USER:
                return deleteUser(uri, null, null);
            case USER_FIREBASE_ID:
                selection = Constants.UserEntry.COL1_FIREBASEID + " =?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                return deleteUser(uri, selection, selectionArgs);
            default:
                createLog("No uri match, something has gone wrong.");
                return 0;
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int match = uriMatcher.match(uri);

        switch (match) {
            case PAYMENT:
                return updatePayment(uri, values, selection, selectionArgs);
            case PAYMENT_ID:
                selection = Constants.PaymentEntry.Col0_ID + " =?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                return updatePayment(uri, values, selection, selectionArgs);
            case PAYMENT_FIREBASE_ID:
                selection = Constants.PaymentEntry.COL1_FIREBASEID + " =?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                return updatePayment(uri, values, selection, selectionArgs);
            case USER:
                return updateUser(uri, values, selection, selectionArgs);
            case USER_ID:
                selection = Constants.UserEntry.Col0_id + " =?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                return updateUser(uri, values, selection, selectionArgs);
            case USER_FIREBASE_ID:
                selection = Constants.UserEntry.COL1_FIREBASEID + " =?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                return updateUser(uri, values, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }


    }

    private String getTableName(Uri uri) {
        SharedPreferences prefs = getContext().getSharedPreferences(Constants.SAVED_DATABASE, Context.MODE_PRIVATE);
        String name = prefs.getString(Constants.SAVED_DATABASE_ID, null);

        int match = uriMatcher.match(uri);
        String tableBaseName = null;
        switch (match) {
            case PAYMENT:
            case PAYMENT_ID:
            case PAYMENT_FIREBASE_ID:
                tableBaseName = Constants.PaymentEntry.TABLE_NAME;
                break;
            case USER:
            case USER_ID:
            case USER_FIREBASE_ID:
                tableBaseName = Constants.UserEntry.TABLE_NAME;
                break;
            case EXCHANGE_RATE:
            case EXCHANGE_RATE_ID:
                tableBaseName = Constants.ExchangeRateEntry.TABLE_NAME;
                break;
            case DATABASES:
            case DATABASES_ID:
            case DATABASES_FIREBASE_ID:
                tableBaseName = Constants.DatabaseEntry.TABLE_NAME;
                break;
            default:
                createLog("There is a problem with the URI. The Uri matcher did not find a match");
                break;
        }

        return tableBaseName + name;
    }

    private Uri insertExchangeRate(Uri uri, ContentValues values) {
        SQLiteDatabase db = exchangeRateDatabaseHelper.getWritableDatabase();
        long id = db.insert(getTableName(uri), null, values);

        return ContentUris.withAppendedId(uri, id);
    }

    private Uri insertPayment(Uri uri, ContentValues values) {
        SQLiteDatabase db = paymentDatabaseHelper.getWritableDatabase();
        long id = db.insert(getTableName(uri), null, values);

        return ContentUris.withAppendedId(uri, id);
    }

    private Uri insertUser(Uri uri, ContentValues values) {
        SQLiteDatabase db = userDatabaseHelper.getWritableDatabase();
        long id = db.insert(getTableName(uri), null, values);

        return ContentUris.withAppendedId(uri, id);
    }

    private int deletePayment(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = paymentDatabaseHelper.getWritableDatabase();

        int id = db.delete(getTableName(uri), selection, selectionArgs);

        if (id < 0) {
            createLog("Failed to update row for: " + uri);
        }

        return id;
    }

    private int deleteUser(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = userDatabaseHelper.getWritableDatabase();

        int id = db.delete(getTableName(uri), selection, selectionArgs);

        if (id < 0) {
            createLog("Failed to update row for: " + uri);
        }

        return id;
    }

    private int updatePayment(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = paymentDatabaseHelper.getWritableDatabase();

        int id = db.update(getTableName(uri), values, selection, selectionArgs);

        if (id < 0) {
            createLog("Failed to update row for: " + uri);
        }

        return id;
    }

    private int updateUser(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = userDatabaseHelper.getWritableDatabase();

        int id = db.update(getTableName(uri), values, selection, selectionArgs);

        if (id < 0) {
            createLog("Failed to update row for: " + uri);
        }

        return id;
    }

    private void createLog(String message) {
        Log.d(DatabaseContentProvider.class.getSimpleName(), message);
    }
}
