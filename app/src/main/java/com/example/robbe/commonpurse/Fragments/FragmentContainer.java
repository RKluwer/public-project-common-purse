package com.example.robbe.commonpurse.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.robbe.commonpurse.CheckConnectivity;
import com.example.robbe.commonpurse.Constants.PaymentEntry;
import com.example.robbe.commonpurse.Constants.UserEntry;
import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.Notes.AddNoteDialog;
import com.example.robbe.commonpurse.Payments.AddPaymentDialog;
import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.SQLite.CursorToArrayConverter;
import com.example.robbe.commonpurse.SQLite.ObjectToContentValues;
import com.example.robbe.commonpurse.SQLite.PaymentDatabaseHelper;
import com.example.robbe.commonpurse.TempImportData;
import com.example.robbe.commonpurse.Users.AddUserActivity;
import com.example.robbe.commonpurse.Users.User;
import com.example.robbe.commonpurse.Users.UserList;
import com.example.robbe.commonpurse.Databases.ChooseDatabaseToJoin;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FragmentContainer extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private String databaseName;
    private static final int LOAD_UPDATED_PAYMENTS = 600;
    private static final int LOAD_DELETED_PAYMENTS = 601;
    private static final int requestCode = 2;

    DatabaseReference paymentDatabaseReference, userDatabaseReference;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        createLog("Creating user: " + requestCode);

        if (requestCode == this.requestCode && resultCode == RESULT_OK) {
            User returnedUser = (User) data.getSerializableExtra(Constants.RETURN_USER);
            createLog(returnedUser.toString());


            if (returnedUser.getPhotoUri() != null) {
                Uri returnedUserUri = Uri.parse(returnedUser.getPhotoUri());
                FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
                StorageReference storageReference = firebaseStorage.getReference().child(databaseName).child(Constants.PHOTO_DATABASE);
                storageReference.child(System.currentTimeMillis() + returnedUserUri.getLastPathSegment())
                        .putFile(returnedUserUri)
                        .addOnSuccessListener(this, taskSnapshot -> {
                            String photoUrl = taskSnapshot.getDownloadUrl().toString();
                            returnedUser.setPhotoId(photoUrl);
                            returnedUser.setPhotoUri(null);

                            if (returnedUser.getFirebaseKey() != null) {
                                updateUserOnline(returnedUser);
                            } else {
                                createUserOnline(returnedUser);
                            }
                        });
            } else {
                if (returnedUser.getFirebaseKey() != null) {
                    updateUserOnline(returnedUser);
                } else {
                    createUserOnline(returnedUser);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_new_user:
                Intent userListIntent = new Intent(this, UserList.class);
                userListIntent.putExtra(Constants.DATABASE_NAME, databaseName);
                userListIntent.putExtra(Constants.USER_DETAIL_VIEW, true);
                startActivity(userListIntent);
                return true;
            case R.id.settings_menu:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
            case R.id.log_out_menu:
                AuthUI.getInstance()
                        .signOut(FragmentContainer.this);
                return true;
            case R.id.choose_database:
                Intent chooseDatabaseIntent = new Intent(this, ChooseDatabaseToJoin.class);
                startActivity(chooseDatabaseIntent);
                return true;
            case R.id.import_data:
                TempImportData.importOldData(databaseName);
                return true;
            case R.id.delete_sql_data:
                getBaseContext().getContentResolver().delete(PaymentEntry.CONTENT_URI_PAYMENTS, null, null);
                getBaseContext().getContentResolver().delete(UserEntry.CONTENT_URI_USERS, null, null);
                return true;
            case R.id.action_go_to_addPayment:
                if (databaseName != null) {
                    AddPaymentDialog addPaymentDialog = new AddPaymentDialog();
                    addPaymentDialog.setDatabase(databaseName);
                    addPaymentDialog.show(getSupportFragmentManager(), "AddPaymentDialog");
                } else {
                    Log.d(Constants.FRAGMENT_CONTAINER_LOG_TAG, "Something has gone wrong. Please contact the developer");
                }

                return true;
            case R.id.action_add_note:
                AddNoteDialog addNoteDialog = new AddNoteDialog();
                addNoteDialog.setDatabaseName(databaseName);
                addNoteDialog.show(getFragmentManager(), "AddNoteDialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        databaseName = intent.getStringExtra("DatabaseName");

        if (savedInstanceState != null) {
            databaseName = savedInstanceState.getString(Constants.DATABASE_NAME);
        } else {
            SharedPreferences prefs = getSharedPreferences(Constants.SAVED_DATABASE, MODE_PRIVATE);
            databaseName = prefs.getString(Constants.SAVED_DATABASE, null);
            createLog("savedInstanceState == null");
        }

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        paymentDatabaseReference = db.getReference()
                .child(databaseName)
                .child(Constants.PAYMENTS);

        userDatabaseReference = db.getReference()
                .child(databaseName)
                .child(Constants.USERS);

        checkPermissions();
        getSupportLoaderManager().restartLoader(LOAD_UPDATED_PAYMENTS, null, FragmentContainer.this);
        getSupportLoaderManager().restartLoader(LOAD_DELETED_PAYMENTS, null, FragmentContainer.this);

        ViewPager viewPager = findViewById(R.id.viewpager);
        CategoryAdapter categoryAdapter = new CategoryAdapter(FragmentContainer.this, getSupportFragmentManager());
        categoryAdapter.setDatabaseName(databaseName);
        viewPager.setAdapter(categoryAdapter);

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        StandingsFragment standingsFragment = categoryAdapter.getStandingsFragment();
                        standingsFragment.startLoadingData();
                        return;
                    case 1:
                        PaymentOverviewLeft paymentOverviewLeft = categoryAdapter.getPaymentOverviewLeft();
                        paymentOverviewLeft.startLoadingAllPayments();
                        return;
                    case 2:
                        PaymentOverviewRight paymentOverviewRight = categoryAdapter.getPaymentOverviewRight();
                        paymentOverviewRight.startLoadingAllPayments();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void saveDataOffline() {
        ArrayList<User> allUserArrayList = new ArrayList<>();
        if (!CheckConnectivity.hasInternetConnection(getBaseContext())) {
            return;
        }

        paymentDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Payment newPayment = dataSnapshot.getValue(Payment.class);
                newPayment.setKey(dataSnapshot.getKey());

                savePaymentOffline(newPayment);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Payment newPayment = dataSnapshot.getValue(Payment.class);
                newPayment.setKey(dataSnapshot.getKey());

                createLog("On user changed has been called for payment: " + newPayment.toString());

                changePaymentOffline(newPayment);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                int id = getBaseContext().getContentResolver().delete(Uri.withAppendedPath(
                        PaymentEntry.CONTENT_URI_PAYMENTS, dataSnapshot.getKey()),
                        null, null);
                createLog("Deleted payment id is: " + id);
                createLog("Deleted payment is: " + dataSnapshot.getValue(Payment.class).toString());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                // Not relevant for this app.
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                createLog(databaseError.getMessage());
                createLog(databaseError.getDetails());
            }
        });

        userDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User newUser = dataSnapshot.getValue(User.class);
                newUser.setFirebaseKey(dataSnapshot.getKey());
                allUserArrayList.add(newUser);

                saveUserOffline(newUser);

                if (newUser.getPhotoId() != null) {
                    createLog("Saving the image in the internal storage");
                    AsyncTaskSaveImage asyncTaskSaveImage = new AsyncTaskSaveImage();
                    asyncTaskSaveImage.execute(Uri.parse(newUser.getPhotoId()));
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                User newUser = dataSnapshot.getValue(User.class);
                newUser.setFirebaseKey(dataSnapshot.getKey());

                createLog("On user changed has been called for user: " + newUser.toString());

                changeUserOffline(newUser);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                int id = getBaseContext().getContentResolver().delete(
                        Uri.withAppendedPath(UserEntry.CONTENT_URI_USERS, dataSnapshot.getKey()),
                                null, null);

                createLog("Deleted user id is: " + id);
                createLog("Deleted user is: " + dataSnapshot.getValue(User.class).toString());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                // Not relevant for this app.
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                createLog(databaseError.getDetails());
                createLog(databaseError.getMessage());
            }
        });

        userDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                SharedPreferences userPrefs = getSharedPreferences(Constants.USERS, MODE_PRIVATE);
                String currentUserName = userPrefs.getString(Constants.CURRENT_USER_NAME, null);
                String currentUserID = userPrefs.getString(Constants.CURRENT_USER_ID, null);
                if (currentUserID != null && CheckConnectivity.hasInternetConnection(getBaseContext())){
                    boolean isInDatabase = false;

                    for (User u: allUserArrayList){
                        if (u.getId().equals(currentUserID)){
                            isInDatabase = true;
                        }
                    }

                    if (!isInDatabase){
                        User newUser = new User();
                        newUser.setName(currentUserName);
                        newUser.setId(currentUserID);

                        Intent intent = new Intent(getBaseContext(), AddUserActivity.class);
                        intent.putExtra(Constants.SEND_USER, newUser);
                        startActivityForResult(intent, requestCode);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateUserOnline(User returnedUser) {
        String key = returnedUser.getFirebaseKey();
        returnedUser.setFirebaseKey(null);

        userDatabaseReference.child(key).setValue(returnedUser);
    }

    private void createUserOnline(User user) {
        userDatabaseReference.push().setValue(user);
    }

    private void savePaymentOffline(Payment payment) {
        Cursor cursor = getBaseContext().getContentResolver().query(
                Uri.withAppendedPath(PaymentEntry.CONTENT_URI_PAYMENTS, payment.getKey()),
                PaymentEntry.PROJECT_ALL,
                null,
                new String[]{payment.getKey()},
                null
        );

        if (cursor == null) {
            createLog("Something has gone wrong with loading the payment");
            return;
        } else if (cursor.getCount() == 0) {
            createLog("This payment is not yet in the database, and will be saved now.");

            Uri returnUri = getBaseContext().getContentResolver().insert(PaymentEntry.CONTENT_URI_PAYMENTS,
                    ObjectToContentValues.getPaymentContentValues(payment, Constants.TRUE));
            createLog("Successfully entered the payment with Uri: " + returnUri);
        }

        cursor.close();
    }

    private void changePaymentOffline(Payment payment) {
        Cursor cursor = getBaseContext().getContentResolver().query(
                Uri.withAppendedPath(PaymentEntry.CONTENT_URI_PAYMENTS, payment.getKey()),
                PaymentEntry.PROJECT_ALL,
                null,
                new String[]{payment.getKey()},
                null
        );

        if (cursor == null) {
            createLog("Something has gone wrong with loading the payment.");
            return;
        } else if (cursor.getCount() == 0) {
            savePaymentOffline(payment);
        } else {
            int id = getBaseContext().getContentResolver().update(Uri.withAppendedPath(PaymentEntry.CONTENT_URI_PAYMENTS, payment.getKey()),
                    ObjectToContentValues.getPaymentContentValues(payment, Constants.TRUE), null, null);

            createLog("Payment with id " + id + " has been updated.");
        }

        cursor.close();
    }

    private void saveUserOffline(User user) {
        Cursor cursor = getBaseContext().getContentResolver().query(
                Uri.withAppendedPath(UserEntry.CONTENT_URI_USERS, user.getFirebaseKey()),
                UserEntry.PROJECT_ALL,
                null,
                new String[]{user.getFirebaseKey()},
                null
        );

        if (cursor == null) {
            createLog("Something has gone wrong with loading the payment");
        } else if (cursor.getCount() == 0) {
            createLog("This payment is not yet in the database, and will be saved now.");

            Uri returnUri = getBaseContext().getContentResolver().insert(UserEntry.CONTENT_URI_USERS,
                    ObjectToContentValues.getUserContentValues(user));
            createLog("Successfully entered the payment with Uri: " + returnUri);
        } else if (cursor.getCount() > 0) {
            createLog("This payment is already in the database");
        }

        if (cursor != null) {
            cursor.close();
        }
    }

    private void changeUserOffline(User user) {
        Cursor cursor = getBaseContext().getContentResolver().query(
                Uri.withAppendedPath(UserEntry.CONTENT_URI_USERS, user.getFirebaseKey()),
                UserEntry.PROJECT_ALL,
                null,
                new String[]{user.getFirebaseKey()},
                null
        );

        if (cursor == null) {
            createLog("Something has gone wrong with loading the payment.");
            return;
        } else if (cursor.getCount() == 0) {
            saveUserOffline(user);
        } else {
            int id = getBaseContext().getContentResolver().update(Uri.withAppendedPath(UserEntry.CONTENT_URI_USERS, user.getFirebaseKey()),
                    ObjectToContentValues.getUserContentValues(user), null, null);

            createLog("User with id " + id + " has been updated.");
        }

        cursor.close();
    }

    private void updateOnlineDatabase(Cursor cursor){
        ArrayList<Payment> paymentList = CursorToArrayConverter.getPaymentArrayList(cursor);

        for (Payment p: paymentList){
            if (p.getKey() != null) {
                String key = p.getKey();
                p.setKey(null);
                paymentDatabaseReference.child(key).setValue(p);
            } else {
                paymentDatabaseReference.push().setValue(p);
            }
        }

    }

    private void deleteOnlineDatabase(Cursor cursor){
        ArrayList<Payment> paymentList = CursorToArrayConverter.getPaymentArrayList(cursor);

        for (Payment p: paymentList){
            if (p.getKey() != null) {
                paymentDatabaseReference.child(p.getKey()).removeValue();
            } else {
                createLog("This payment has not yet been in the online database.");
            }
        }
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                createLog("Permission to write the database is granted");
                saveDataOffline();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(FragmentContainer.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "This app needs to save your data on the phone. " +
                            "Please allow this in the settings.", Toast.LENGTH_LONG).show();
                } else {
                    ActivityCompat.requestPermissions(FragmentContainer.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            }
        }
    }

    private String getFileName(File directory, String fileName) {
        File[] listFiles = directory.listFiles();
        try {
            createLog("Directory file size: " + listFiles.length);
            for (File f : listFiles) {
                if (f.getName().contains(fileName)) {
                    return f.getName();
                }
            }
        } catch (NullPointerException e) {
            createLog("No files found. ");
        }
        return fileName;
    }

    private File getTempFile(Uri pictureUri) {
        File directory = Environment.getExternalStorageDirectory();
        File filePathSegment = new File(directory, pictureUri.getLastPathSegment());

        return new File(directory, getFileName(directory, filePathSegment.getName()));
    }

    public String getDatabaseName() {
        return databaseName;
    }

    private void createLog(String message) {
        Log.d(Constants.FRAGMENT_CONTAINER_LOG_TAG, message);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Constants.DATABASE_NAME, databaseName);
        createLog("SavedInstanceState called");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createLog("Permission is granted");
                saveDataOffline();
            } else {
                Toast.makeText(this, "You have to enable the permissions in order to continue", Toast.LENGTH_SHORT).show();
                checkPermissions();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id){
            case LOAD_UPDATED_PAYMENTS:
                return new CursorLoader(
                        this,
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        PaymentEntry.COL15_ISPOSTED + " =?",
                        new String[]{String.valueOf(Constants.FALSE)},
                        null
                );
            case LOAD_DELETED_PAYMENTS:
                return new CursorLoader(
                        this,
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        PaymentEntry.COL15_ISPOSTED + " =?",
                        new String[]{String.valueOf(Constants.TO_BE_DELETED)},
                        null
                );
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()){
            case LOAD_UPDATED_PAYMENTS:
                updateOnlineDatabase(data);
                return;
            case LOAD_DELETED_PAYMENTS:
                deleteOnlineDatabase(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskSaveImage extends AsyncTask<Uri, Void, Void> {

        @Override
        protected Void doInBackground(Uri... uris) {
            File tempFile = getTempFile(uris[0]);

            if (tempFile.exists()) {
                createLog("File has been saved before. File exists");
                return null;
            } else {
                createLog("File has not been cached. File does not exist yet.");

                try {
                    File directory = Environment.getExternalStorageDirectory();
                    File file = File.createTempFile(tempFile.getName(), null, directory);

                    if (!file.mkdir()) {
                        final boolean mkdirs = file.mkdir();
                        createLog("Directory was created: " + mkdirs);
                    }

                    FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
                    StorageReference storageReference = firebaseStorage.getReference();
                    storageReference.child(uris[0].getLastPathSegment()).getFile(file)
                            .addOnSuccessListener(taskSnapshot ->
                                    createLog("file was stored in the cache: "
                                            + file.getAbsolutePath()
                                            + " and file name is: "
                                            + file.getName()))
                            .addOnFailureListener(e ->
                                    createLog("File was not stored in cache, an error has occurred. "
                                            + e.getMessage()));

                } catch (IOException | NullPointerException e) {
                    createLog("Something has gone wrong when caching the file: " + e.getMessage());

                }
            }

            return null;
        }
    }
}
