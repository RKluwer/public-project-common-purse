package com.example.robbe.commonpurse.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.R;

/**
 * Created by robbe on 18-11-2017.
 */

public class PaymentOverviewLeft extends PaymentOverviewRight {
    public PaymentOverviewLeft(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        setSpinnerLayout(false);
        Log.d("PaymentOverviewLeft", "setupSpinnerLayout called");

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void loadPayments() {
        getActivity().getSupportLoaderManager().initLoader(LOAD_VISIBLE_PAYMENTS, null, PaymentOverviewLeft.this);
    }

    private void createLog(String message){
        Log.d("PaymentOverviewLeft", message);
    }
}