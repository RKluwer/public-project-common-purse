package com.example.robbe.commonpurse;

import android.util.Log;

import com.example.robbe.commonpurse.Payments.Payment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TempImportData {
    public static void importOldData(String dbName){
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        DatabaseReference luciaDbRef = fdb.getReference().child("Lucia");
        DatabaseReference robDbRef = fdb.getReference().child("Robbert");
        DatabaseReference newDbRef = fdb.getReference().child(dbName).child("Payments");

        ArrayList<Payment> luciaPayments = new ArrayList<>();
        ArrayList<Payment> robbertPayments = new ArrayList<>();
        ArrayList<Payment> newPayments = new ArrayList<>();

        newDbRef.removeValue((databaseError, databaseReference) -> {
            luciaDbRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Payment newPayment = dataSnapshot.getValue(Payment.class);
                    if (!luciaPayments.contains(newPayment)) {
                        luciaPayments.add(newPayment);
                    } else {
                        Log.d("TempImportData", "This payment is already in the list." +
                                "Something must have gone wrong");
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            luciaDbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (Payment p: luciaPayments){
                        Payment newLuciaPayment = new Payment();
                        Payment newRobbertPayment = new Payment();

                        newLuciaPayment.setAmount(p.getTotalAmount() - p.getAmount());
                        newRobbertPayment.setAmount(p.getAmount());

                        newLuciaPayment.setApproved(true);
                        newRobbertPayment.setApproved(true);

                        newLuciaPayment.setBasePayment(false);
                        newRobbertPayment.setBasePayment(true);

                        newLuciaPayment.setDescription(p.getDescription());
                        newRobbertPayment.setDescription(p.getDescription());

                        newLuciaPayment.setmCreatorRole(Constants.USER_ROLE_NORMAL);
                        newRobbertPayment.setmCreatorRole(Constants.USER_ROLE_NORMAL);

                        newLuciaPayment.setCurrency(p.getCurrency());
                        newRobbertPayment.setCurrency(p.getCurrency());

                        newLuciaPayment.setmOriginalAmount(p.getmOriginalAmount());
                        newRobbertPayment.setmOriginalAmount(p.getmOriginalAmount());

                        newLuciaPayment.setmTime(p.getmTime());
                        newRobbertPayment.setmTime(p.getmTime());

                        newLuciaPayment.setTotalAmount(p.getTotalAmount());
                        newRobbertPayment.setTotalAmount(p.getTotalAmount());

                        newLuciaPayment.setReceiver("Lucia Majerikova");
                        newRobbertPayment.setReceiver("Robbert Kluwer");

                        if (p.getmCreator() != null){
                            if (p.getmCreator().equals("Anonymous") || p.getmCreator().equals("Lucia")) {
                                newLuciaPayment.setmCreator("Lucia Majerikova");
                                newRobbertPayment.setmCreator("Lucia Majerikova");
                            }  else if (p.getmCreator().equals("Robbert Kluwer")){
                                newLuciaPayment.setmCreator("Robbert Kluwer");
                                newRobbertPayment.setmCreator("Robbert Kluwer");
                            }
                        }

                        if (p.getUser() != null){
                            if (p.getUser().equals("Anonymous") || p.getUser().equals("Lucia")){
                                newLuciaPayment.setUser("Lucia Majerikova");
                                newRobbertPayment.setUser("Lucia Majerikova");
                            } else if (p.getUser().equals("Robbert Kluwer")){
                                newLuciaPayment.setUser("Robbert Kluwer");
                                newRobbertPayment.setUser("Robbert Kluwer");
                            }
                        }

                        newPayments.add(newLuciaPayment);
                        newPayments.add(newRobbertPayment);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            robDbRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Payment newPayment = dataSnapshot.getValue(Payment.class);
                    robbertPayments.add(newPayment);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            robDbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (Payment p: robbertPayments){
                        Payment newLuciaPayment = new Payment();
                        Payment newRobbertPayment = new Payment();

                        newRobbertPayment.setAmount(p.getTotalAmount() - p.getAmount());
                        newLuciaPayment.setAmount(p.getAmount());

                        newLuciaPayment.setApproved(true);
                        newRobbertPayment.setApproved(true);

                        newLuciaPayment.setBasePayment(false);
                        newRobbertPayment.setBasePayment(true);

                        newLuciaPayment.setDescription(p.getDescription());
                        newRobbertPayment.setDescription(p.getDescription());

                        newLuciaPayment.setmCreator(Constants.USER_ROLE_NORMAL);
                        newRobbertPayment.setmCreator(Constants.USER_ROLE_NORMAL);

                        newLuciaPayment.setCurrency(p.getCurrency());
                        newRobbertPayment.setCurrency(p.getCurrency());

                        newLuciaPayment.setmOriginalAmount(p.getmOriginalAmount());
                        newRobbertPayment.setmOriginalAmount(p.getmOriginalAmount());

                        newLuciaPayment.setmTime(p.getmTime());
                        newRobbertPayment.setmTime(p.getmTime());

                        newLuciaPayment.setTotalAmount(p.getTotalAmount());
                        newRobbertPayment.setTotalAmount(p.getTotalAmount());

                        newLuciaPayment.setReceiver("Lucia Majerikova");
                        newRobbertPayment.setReceiver("Robbert Kluwer");

                        if (p.getmCreator() != null){
                            if (p.getmCreator().equals("Anonymous") || p.getmCreator().equals("Robbert")) {
                                newLuciaPayment.setmCreator("Robbert Kluwer");
                                newRobbertPayment.setmCreator("Robbert Kluwer");
                            }  else if (p.getmCreator().equals("Lucia")){
                                newLuciaPayment.setmCreator("Lucia Majerikova");
                                newRobbertPayment.setmCreator("Lucia Majerikova");
                            }
                        }

                        if (p.getUser() != null){
                            if (p.getUser().equals("Anonymous") || p.getUser().equals("Robbert")){
                                newLuciaPayment.setUser("Robbert Kluwer");
                                newRobbertPayment.setUser("Robbert Kluwer");
                            } else if (p.getUser().equals("Lucia")){
                                newLuciaPayment.setUser("Lucia Majerikova");
                                newRobbertPayment.setUser("Lucia Majerikova");
                            }
                        }

                        newPayments.add(newLuciaPayment);
                        newPayments.add(newRobbertPayment);
                    }

                    for (Payment p: newPayments){
                        newDbRef.push().setValue(p);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        });
    }
}
