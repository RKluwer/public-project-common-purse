package com.example.robbe.commonpurse;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.robbe.commonpurse.Users.AddUserActivity;
import com.example.robbe.commonpurse.Users.User;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CheckConnectivity {

    private boolean isExistingUser;

    public static boolean hasInternetConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            NetworkInfo info = cm.getActiveNetworkInfo();

            Log.d(Constants.CHECK_CONNECTIVITY_LOG_TAG, "Internet is on: " + info.isConnected());
            return info.isConnected();
        } catch (NullPointerException e){
            Log.d(Constants.CHECK_CONNECTIVITY_LOG_TAG, "Cannot find the internet connection");
            return false;
        }
    }
}
