package com.example.robbe.commonpurse.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.robbe.commonpurse.Constants.UserEntry;

public class UserDatabaseHelper extends SQLiteOpenHelper {
    String tableName;

    public UserDatabaseHelper(Context context, String name) {
        super(context, name, null, 1);
        tableName = name;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + tableName + " ("
                + UserEntry.Col0_id + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + UserEntry.COL1_FIREBASEID + " TEXT NOT NULL, "
                + UserEntry.COL2_FIRSTNAME + " TEXT, "
                + UserEntry.COL3_LASTNAME + " TEXT, "
                + UserEntry.COL4_FULLNAME + " TEXT, "
                + UserEntry.COL5_USERID + " TEXT NOT NULL, "
                + UserEntry.COL6_EMAIL + " TEXT NOT NULL, "
                + UserEntry.COL7_USERROLE + " TEXT NOT NULL, "
                + UserEntry.COL8_TIMECREATED + " INTEGER NOT NULL, "
                + UserEntry.COL9_PHOTOURI + " TEXT)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF TABLE EXISTS " + tableName);
        onCreate(db);
    }
}
