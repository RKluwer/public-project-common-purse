package com.example.robbe.commonpurse.Payments;

import android.annotation.SuppressLint;

/**
 * Created by robbe on 18-11-2017.
 */

public class Payment {

    private String mCurrency;
    private String mUser;
    private String mCreator;
    private String mCreatorRole;
    private String mReceiver;
    private float mAmountInCZK;
    private float mTotalAmount;
    private float mOriginalAmount;
    private String mDescription;
    private long mTime;
    private String mPhotoId;
    private boolean mApproved;
    private boolean mBasePayment;
    private String mKey;

    public Payment(){};

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String mCurrency) {
        this.mCurrency = mCurrency;
    }

    public String getUser(){ return mUser; }

    public float getAmount() {
        return mAmountInCZK;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getKey() {return mKey;}

    public void setKey(String mKey) {this.mKey = mKey;}

    public long getmTime() {
        return mTime;
    }

    public float getmOriginalAmount() {
        return mOriginalAmount;
    }

    public String getPhotoId() {
        return mPhotoId;
    }

    public void setUser(String mUser) {
        this.mUser = mUser;
    }

    public void setAmount(float mAmount) {
        this.mAmountInCZK = mAmount;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setmTime(long mTime) {
        this.mTime = mTime;
    }

    public void setmOriginalAmount(float mOriginalAmount) {
        this.mOriginalAmount = mOriginalAmount;
    }

    public float getTotalAmount() {
        return mTotalAmount;
    }

    public void setTotalAmount(float mTotalAmount) {
        this.mTotalAmount = mTotalAmount;
    }

    public String getmCreator() {
        return mCreator;
    }

    public void setmCreator(String mCreator) {
        this.mCreator = mCreator;
    }

    public void setPhotoId(String mPhotoId) {
        this.mPhotoId = mPhotoId;
    }

    public String getmCreatorRole() {
        return mCreatorRole;
    }

    public void setmCreatorRole(String mCreatorRole) {
        this.mCreatorRole = mCreatorRole;
    }

    public String getReceiver() {
        return mReceiver;
    }

    public void setReceiver(String mReceiver) {
        this.mReceiver = mReceiver;
    }

    public boolean isApproved() {
        return mApproved;
    }

    public void setApproved(boolean mApproved) {
        this.mApproved = mApproved;
    }

    public boolean isBasePayment() {
        return mBasePayment;
    }

    public void setBasePayment(boolean basePayment) {
        this.mBasePayment = basePayment;
    }

    public String toShortString(){
        String[] firstName = this.getReceiver().split(" ");
        @SuppressLint("DefaultLocale") String formattedAmount = String.format("%20.2f", this.getAmount());

        return firstName[0] + formattedAmount;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Payment: ");
        builder.append(this.getDescription());
        builder.append(" for: ");
        builder.append(this.getAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("At time: ");
        builder.append(this.getmTime());
        builder.append(System.getProperty("line.separator"));
        builder.append("From: ");
        builder.append(this.getUser());
        builder.append(" to: ");
        builder.append(this.getReceiver());

        return builder.toString();
    }

    public String toLongString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Your payment is: ");
        builder.append(System.getProperty("line.separator"));
        builder.append("User: ");
        builder.append(this.getUser());
        builder.append(System.getProperty("line.separator"));
        builder.append("Creator: ");
        builder.append(this.getmCreator());
        builder.append(System.getProperty("line.separator"));
        builder.append("Creator role: ");
        builder.append(this.getmCreatorRole());
        builder.append(System.getProperty("line.separator"));
        builder.append("Receiver: ");
        builder.append(this.getReceiver());
        builder.append(System.getProperty("line.separator"));
        builder.append("Currency: ");
        builder.append(this.getCurrency());
        builder.append(System.getProperty("line.separator"));
        builder.append("Amount: ");
        builder.append(this.getAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("Total Amount: ");
        builder.append(this.getTotalAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("Amount in other currency: ");
        builder.append(this.getmOriginalAmount());
        builder.append(System.getProperty("line.separator"));
        builder.append("Description: ");
        builder.append(this.getDescription());
        builder.append(System.getProperty("line.separator"));
        builder.append("Time; ");
        builder.append(this.getmTime());
        builder.append(System.getProperty("line.separator"));
        builder.append("Has photo: ");
        builder.append(getPhotoId() != null);
        builder.append(System.getProperty("line.separator"));
        builder.append("Is approved: ");
        builder.append(isApproved());

        return builder.toString();
    }

    public void updateValues(Payment updatedPayment) {
        mUser = updatedPayment.getUser();
        mCreator = updatedPayment.getmCreator();
        mCurrency = updatedPayment.getCurrency();
        mReceiver = updatedPayment.getReceiver();
        mAmountInCZK = updatedPayment.getAmount();
        mTotalAmount = updatedPayment.getTotalAmount();
        mOriginalAmount = updatedPayment.getmOriginalAmount();
        mDescription = updatedPayment.getDescription();
        mTime = updatedPayment.getmTime();
        mPhotoId = updatedPayment.getPhotoId();
    }
}
