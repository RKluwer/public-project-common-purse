package com.example.robbe.commonpurse.Notes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.robbe.commonpurse.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by robbe on 20-2-2018.
 */

public class NoteAdapter extends ArrayAdapter<Note> {


    public NoteAdapter(@NonNull Context context, int resource, @NonNull List<Note> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listView = convertView;
        if (listView == null){
            listView = LayoutInflater.from(getContext()).inflate(R.layout.note_list_item, parent, false);
        }

        final Note currentNote = getItem(position);

        TextView userTv = listView.findViewById(R.id.note_user_tv);
        userTv.setText(currentNote.getUser());

        TextView titleTv = listView.findViewById(R.id.note_list_title_tv);
        titleTv.setText(currentNote.getTitle());

        TextView timeTv = listView.findViewById(R.id.note_time_and_date_tv);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        Date resultDate = new Date(currentNote.getTimeAmended());
        timeTv.setText(sdf.format(resultDate));

        TextView currencyTv = listView.findViewById(R.id.note_currency_tv);
        currencyTv.setText(currentNote.getCurrency());

        TextView amountTv = listView.findViewById(R.id.note_amount_tv);
        amountTv.setText(String.format("%.2f", currentNote.getAmount()));

        RelativeLayout layout = listView.findViewById(R.id.note_list_layout);

        if (currentNote.getUser().equals("Lucia")){
            layout.setBackgroundColor(listView.getResources().getColor(R.color.lucia_background));
        } else {
            layout.setBackgroundColor(listView.getResources().getColor(R.color.robbert_background));
        }
        return listView;
    }
}
