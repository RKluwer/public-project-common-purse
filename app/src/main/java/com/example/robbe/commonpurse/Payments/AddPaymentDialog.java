package com.example.robbe.commonpurse.Payments;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.robbe.commonpurse.CheckConnectivity;
import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.JsonHelper;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.SQLite.CursorToArrayConverter;
import com.example.robbe.commonpurse.SQLite.ObjectToContentValues;
import com.example.robbe.commonpurse.Users.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.example.robbe.commonpurse.Constants.ExchangeRateEntry;
import com.example.robbe.commonpurse.Constants.UserEntry;
import com.example.robbe.commonpurse.Constants.PaymentEntry;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by robbe on 23-11-2017.
 */

public class AddPaymentDialog extends DialogFragment implements LoaderCallbacks<Cursor> {

    private static final int LOAD_ALL_EXCHANGE_RATES = 300;
    private static final int LOAD_ALL_USERS = 310;
    private static final int LOAD_SPECIFIC_PAYMENT = 320;

    private User currentUser;

    private String mCurrencyValue = null;
    private String mCurrencyName;
    private ArrayList<String> keyList;
    private long time;
    public static final String EXCHANGE_RATE_URL_PART_1 = "http://free.currencyconverterapi.com/api/v5/convert?q=";
    public static final String EXCHANGE_RATE_URL_PART_2 = "_";
    public static final String EXCHANGE_RATE_URL_PART_3 = "&compact=y";

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_SAVED_IMAGE = 2;

    private EditText mAmountEditText, mDescriptionEditText;
    private TextView mExchangeRateTextView, mShowCurrentUserTextView;
    private ImageView mChosenPhotoImageView;

    private String userName = null;
    private Uri mSelectedPictureUri;
    private Uri mPhotoUrl = null;
    private String database;
    private boolean firstPayment = true;

    private ArrayList<User> userArrayList = new ArrayList<>();
    ArrayList<User> selectedUserList;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference paymentDatabaseReference;
    private StorageReference mPhotoStorageReference;
    private Spinner mCurrencySpinner;

    public AddPaymentDialog() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return createPaymentDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // Setting the cursor to the first position in the amount edit text field
        mAmountEditText.setSelection(mAmountEditText.getText().length());
        mAmountEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void setupSpinner() {

        ArrayAdapter currencySpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.array_currencies,
                android.R.layout.simple_spinner_item);

        mCurrencySpinner.setAdapter(currencySpinnerAdapter);
        mCurrencySpinner.setPrompt("Currency");

        // Setting the default value to the chosen default currency.
        try {
            int spinnerPosition = currencySpinnerAdapter.getPosition(getDefaultCurrency());
            mCurrencySpinner.setSelection(spinnerPosition);
        } catch (Exception e) {
            createLog(e.getMessage());
        }

        currencySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mCurrencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String selection = (String) adapterView.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getResources().getString(R.string.CHF))) {
                        mCurrencyName = getResources().getString(R.string.CHF);
                    } else if (selection.equals(getResources().getString(R.string.DKK))) {
                        mCurrencyName = getResources().getString(R.string.DKK);
                    } else if (selection.equals(getResources().getString(R.string.EUR))) {
                        mCurrencyName = getResources().getString(R.string.EUR);
                    } else if (selection.equals(getResources().getString(R.string.GBP))) {
                        mCurrencyName = getResources().getString(R.string.GBP);
                    } else if (selection.equals(getResources().getString(R.string.HUF))) {
                        mCurrencyName = getResources().getString(R.string.HUF);
                    } else if (selection.equals(getResources().getString(R.string.NOK))) {
                        mCurrencyName = getResources().getString(R.string.NOK);
                    } else if (selection.equals(getResources().getString(R.string.PLN))) {
                        mCurrencyName = getResources().getString(R.string.PLN);
                    } else if (selection.equals(getResources().getString(R.string.THB))) {
                        mCurrencyName = getResources().getString(R.string.THB);
                    } else if (selection.equals(getResources().getString(R.string.USD))) {
                        mCurrencyName = getResources().getString(R.string.USD);
                    } else if (selection.equals("CZK")) {
                        mCurrencyName = getResources().getString(R.string.CZK);
                    } else {
                        mCurrencyName = getDefaultCurrency();
                    }
                    if (!mCurrencyName.equals(getDefaultCurrency())) {
                        getCurrency(EXCHANGE_RATE_URL_PART_1 + mCurrencyName + EXCHANGE_RATE_URL_PART_2
                                + getDefaultCurrency() + EXCHANGE_RATE_URL_PART_3);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mCurrencyName = getDefaultCurrency();
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            // TODO Get the picture and display it
        } else if (requestCode == REQUEST_SAVED_IMAGE && resultCode == RESULT_OK) {
            mSelectedPictureUri = data.getData();
            Toast.makeText(getActivity(), "Picture uri: " + mSelectedPictureUri, Toast.LENGTH_SHORT).show();

            mChosenPhotoImageView.setVisibility(View.VISIBLE);
            mChosenPhotoImageView.setImageURI(mSelectedPictureUri);
        }
    }

    public Dialog createPaymentDialog() {

        final Builder builder = new Builder(getActivity());

        final LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.add_payment_dialog, null);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        FirebaseStorage mFirebaseStorage = FirebaseStorage.getInstance();
        paymentDatabaseReference = mFirebaseDatabase.getReference().child(database).child(Constants.PAYMENTS);
        mPhotoStorageReference = mFirebaseStorage.getReference().child(database).child(Constants.PHOTO_DATABASE);
        mCurrencySpinner = dialogView.findViewById(R.id.currency_spinner);
        mChosenPhotoImageView = dialogView.findViewById(R.id.show_image_view);

        mAmountEditText = dialogView.findViewById(R.id.amount_edit_text_dialog);
        mDescriptionEditText = dialogView.findViewById(R.id.description_edit_text_dialog);
        mExchangeRateTextView = dialogView.findViewById(R.id.exchange_rate_text_view);
        ImageButton mPhotoMakerButton = dialogView.findViewById(R.id.photo_button);
        ImageButton mChoosePhotoButton = dialogView.findViewById(R.id.image_button);
        ImageButton mChangeCreatorButton = dialogView.findViewById(R.id.change_user_button);
        mShowCurrentUserTextView = dialogView.findViewById(R.id.add_payment_display_user);

        Button mSplitByAmountButton = dialogView.findViewById(R.id.add_payment_split_by_amount);
        Button splitEquallyButton = dialogView.findViewById(R.id.add_payment_split_by_percentage);
        selectedUserList = new ArrayList<>();

        // Setting the max length of the description to 40 characters.
        InputFilter[] inputFilter = new InputFilter[1];
        inputFilter[0] = new InputFilter.LengthFilter(40);
        mDescriptionEditText.setFilters(inputFilter);

        time = System.currentTimeMillis();

        // To determine what user is the one on the phone.
        getActivity().getSupportLoaderManager().restartLoader(LOAD_ALL_USERS, null, AddPaymentDialog.this);

        // If there is a Payment that needs to be changed, the keyList is not null.
        // The keyList contains all the keys of te payments that need to be changed.
        if (keyList != null) {
            Bundle bundle = new Bundle();
            bundle.putStringArray(Constants.PAYMENT_FIREBASE_KEY, new String[]{keyList.get(0)});

            getActivity().getSupportLoaderManager().restartLoader(LOAD_SPECIFIC_PAYMENT, bundle, AddPaymentDialog.this);
        }

        // Click on the user icon and change the userName of the payment.
        mChangeCreatorButton.setOnClickListener(view -> {
            selectNewUser();
        });

        // Click on the camera icon and start a camera intent.
        mPhotoMakerButton.setOnClickListener(view -> {
            Intent makePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            makePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, true);
            startActivityForResult(makePhotoIntent, REQUEST_IMAGE_CAPTURE);
            // currently not active;
        });

        // Click on the picture icon and start an intent to choose a picture from the data.
        mChoosePhotoButton.setOnClickListener(view -> {
            Intent getExistingPhotoIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getExistingPhotoIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            getExistingPhotoIntent.setType("image/jpeg");
            startActivityForResult(getExistingPhotoIntent, REQUEST_SAVED_IMAGE);
        });

        builder.setView(dialogView);

        splitEquallyButton.setOnClickListener(view -> {
            if (!CheckConnectivity.hasInternetConnection(getActivity())) {
                return;
            }

            if (mAmountEditText.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "You have to enter an amount for this Payment.", Toast.LENGTH_SHORT).show();
                return;
            }

            float totalAmount = Float.valueOf(mAmountEditText.getText().toString());
            BigDecimal individualAmount = new BigDecimal(totalAmount / userArrayList.size());
            for (User u : userArrayList) {
                addNewPayment(individualAmount.floatValue(), u.getName());
            }
            createLog("Number of created payments: " + selectedUserList.size());
            dismiss();
        });

        mSplitByAmountButton.setOnClickListener(view -> {
            if (mAmountEditText.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "You have to enter an amount for this Payment.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (CheckConnectivity.hasInternetConnection(getActivity())) {
                selectUsersToSplit();
            }
        });

        setupSpinner();

        return builder.create();
    }

    private void addNewPayment(float individualAmount, String receiver) {
        createLog("Add new payment called");
        Payment newPayment = new Payment();
        if (mAmountEditText.getText().toString().length() != 0) {

            // Setting the amount values. If another currency is used, the currency values as well
            // as the original amount are set.
            float mTotalAmount = Float.parseFloat(mAmountEditText.getText().toString());
            if (mCurrencyValue != null) {
                float mConvertedAmount = mTotalAmount;
                mTotalAmount = mTotalAmount * Float.parseFloat(mCurrencyValue);
                newPayment.setTotalAmount(mTotalAmount);
                newPayment.setmOriginalAmount(mConvertedAmount);
                newPayment.setCurrency(mCurrencyName);
                createLog("Amount is: " + individualAmount);
            } else {
                mCurrencyName = getDefaultCurrency();
                newPayment.setTotalAmount(mTotalAmount);
                newPayment.setCurrency(mCurrencyName);
            }

            // The description value of the payment is set here.
            String mDescription = mDescriptionEditText.getText().toString().trim();
            if (mDescription.length() == 0) {
                mDescription = "No description";
            }
            newPayment.setDescription(mDescription);

            // The payment is created as long as the individual amount is not 0.
            if (individualAmount != 0) {
                newPayment.setUser(userName);
                newPayment.setmCreator(currentUser.getName());
                newPayment.setmCreatorRole(currentUser.getRole());
                newPayment.setReceiver(receiver);
                newPayment.setAmount(individualAmount);
                newPayment.setmTime(time);
                newPayment.setApproved(true);

                // Setting base payment. Only one payment can be the base payment as this payment
                // is being shown in the views.
                newPayment.setBasePayment(firstPayment);
                if (newPayment.isBasePayment()) {
                    firstPayment = false;
                }

                // When a photo is selected for the first time or already existing.
                // The photo is first uploaded and then the URL is saved in a String.
                if (mSelectedPictureUri != null && mPhotoUrl == null) {
                    mPhotoStorageReference.child(mSelectedPictureUri.getLastPathSegment())
                            .putFile(mSelectedPictureUri)
                            .addOnSuccessListener(getActivity(), taskSnapshot -> {
                                mPhotoUrl = taskSnapshot.getDownloadUrl();
                                newPayment.setPhotoId(mPhotoUrl.toString());
                                makeNewEntry(newPayment);
                            });
                } else if (mPhotoUrl != null) {
                    newPayment.setPhotoId(mPhotoUrl.toString());
                    makeNewEntry(newPayment);
                } else {
                    makeNewEntry(newPayment);
                }

            } else
                Toast.makeText(getActivity(), "The payment amount cannot be 0.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "You have not entered an amount!", Toast.LENGTH_SHORT).show();
        }

    }

    private String getDefaultCurrency() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return preferences.getString("currency_pref", "EUR");
    }

    private void setExchangeRateInView() {
        mExchangeRateTextView.setVisibility(View.VISIBLE);
        String exchangeRateText = "The current exchange rate is: 1 "
                + mCurrencyName
                + " = "
                + mCurrencyValue
                + " "
                + getDefaultCurrency();
        mExchangeRateTextView.setText(exchangeRateText);
    }

    private void selectNewUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String[] userArray = new String[userArrayList.size()];
        for (int i = 0; i < userArrayList.size(); i++) {
            User u = userArrayList.get(i);
            userArray[i] = u.getName();
        }
        builder.setTitle("For who do you create this payment?");
        builder.setItems(userArray, (dialog, which) -> {
            userName = userArray[which];
            mShowCurrentUserTextView.setVisibility(View.VISIBLE);
            mShowCurrentUserTextView.setText(getString(R.string.you_are_making_this_payment_for) + " " + userName);
            createLog("Selected user is: " + userName);
        });
        builder.show();
    }

    private void selectUsersToSplit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String[] userArray = new String[userArrayList.size()];
        for (int i = 0; i < userArrayList.size(); i++) {
            User u = userArrayList.get(i);
            userArray[i] = u.getName();
        }
        builder.setTitle("For who do you create this payment?");
        builder.setMultiChoiceItems(userArray, null, (dialog, which, isChecked) -> {
            if (isChecked) {
                for (User u : userArrayList) {
                    if (u.getName().equals(userArray[which])) {
                        selectedUserList.add(u);
                    }
                }
                createLog("UserList length is: " + selectedUserList.size());
            } else {
                for (User u : userArrayList) {
                    if (u.getName().equals(userArray[which])) {
                        selectedUserList.remove(u);
                    }
                }
                createLog("UserList length is: " + selectedUserList.size());
            }
        });
        builder.setPositiveButton("OK", (dialog, which) -> {
            if (selectedUserList.size() > 0) {
                float totalAmount = Float.valueOf(mAmountEditText.getText().toString());
                BigDecimal individualAmount = new BigDecimal(totalAmount / selectedUserList.size());
                for (User u : selectedUserList) {
                    addNewPayment(individualAmount.floatValue(), u.getName());
                }
                createLog("Number of created payments: " + selectedUserList.size());
                dismiss();
            } else {
                Toast.makeText(getActivity(), "You have not selected anyone to share the costs with", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dismiss());
        builder.show();
    }


    public void makeNewEntry(Payment payment) {
        if (keyList != null && selectedUserList.size() < keyList.size()) {
            int unselectedNumber = keyList.size() - selectedUserList.size();

            for (int i = 0; i < unselectedNumber; i++) {
                if (!CheckConnectivity.hasInternetConnection(getActivity())) {
                    getActivity().getContentResolver().update(Uri.withAppendedPath(
                            PaymentEntry.CONTENT_URI_PAYMENTS, keyList.get(0)),
                            ObjectToContentValues.getPaymentContentValues(payment, Constants.TO_BE_DELETED),
                            null, null);
                } else {
                    paymentDatabaseReference.child(keyList.get(0)).removeValue();
                    keyList.remove(0);
                }
            }
            createLog("Number of payments deleted from the database: " + unselectedNumber);
        }

        // If the keyList is null, or if the keyList does not contain any values, a new payment
        // needs to be made.
        //
        if (keyList == null || keyList.size() == 0) {
            if (!CheckConnectivity.hasInternetConnection(getActivity())) {
                getActivity().getContentResolver().update(Uri.withAppendedPath(
                        PaymentEntry.CONTENT_URI_PAYMENTS, keyList.get(0)),
                        ObjectToContentValues.getPaymentContentValues(payment, Constants.FALSE),
                        null, null);
            } else {
                if (!CheckConnectivity.hasInternetConnection(getActivity())) {
                    getActivity().getContentResolver().insert(PaymentEntry.CONTENT_URI_PAYMENTS,
                            ObjectToContentValues.getPaymentContentValues(payment, Constants.FALSE));
                } else {
                    paymentDatabaseReference.push().setValue(payment);
                    createLog("New payment: " + payment.toString());
                }
            }
        } else {
            String key = keyList.get(0);
            paymentDatabaseReference.child(key).setValue(payment);
            keyList.remove(0);
        }
    }

    public void setPaymentInView(Cursor data) {
        Payment currentPayment = CursorToArrayConverter.getFirstPayment(data);

        if (currentPayment.getmOriginalAmount() == 0) {
            mAmountEditText.setText(String.valueOf(currentPayment.getTotalAmount()));
        } else {
            mAmountEditText.setText(String.valueOf(currentPayment.getmOriginalAmount()));
        }

        mDescriptionEditText.setText(currentPayment.getDescription());

        if (currentPayment.getPhotoId() != null) {
            mChosenPhotoImageView.setVisibility(View.VISIBLE);
            Glide.with(getActivity())
                    .load(Uri.parse(currentPayment.getPhotoId()))
                    .into(mChosenPhotoImageView);
        }

        time = currentPayment.getmTime();

    }

    public void setDatabase(String database) {
        this.database = database;
    }

    private void getCurrentUser(Cursor data) {
        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.USERS, Context.MODE_PRIVATE);
        String uId = prefs.getString(Constants.CURRENT_USER_ID, null);

        userArrayList = CursorToArrayConverter.getUserArrayList(data);

        for (User u : userArrayList) {
            if (u != null && u.getId().equals(uId)) {
                currentUser = u;
                setUserName(u.getName());
            }
        }
    }

    public void setUserName(String userName) {
        this.userName = userName;
        createLog("User payment is made for is: " + userName);
    }

    public void setKeys(ArrayList<String> keyList) {
        if (keyList == null) {
            this.keyList = keyList;
        }
    }

    private void createLog(String message) {
        Log.d("AddPaymentDialog", message);
    }

    @Override
    public void onStop() {
        mSelectedPictureUri = null;
        super.onStop();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOAD_ALL_EXCHANGE_RATES:
                return new CursorLoader(
                        getActivity(),
                        ExchangeRateEntry.CONTENT_URI_EXCHANGE_RATE,
                        ExchangeRateEntry.PROJECT_ALL,
                        ExchangeRateEntry.COL2_FOREIGN_CURRENCY + " =?",
                        args.getStringArray(Constants.EXCHANGE_RATE),
                        ExchangeRateEntry.COL4_TIME_CREATED + " DESC"
                );
            case LOAD_ALL_USERS:
                return new CursorLoader(
                        getActivity(),
                        UserEntry.CONTENT_URI_USERS,
                        UserEntry.PROJECT_ALL,
                        null,
                        null,
                        null
                );
            case LOAD_SPECIFIC_PAYMENT:
                return new CursorLoader(
                        getActivity(),
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        PaymentEntry.COL1_FIREBASEID + " =?",
                        args.getStringArray(Constants.PAYMENT_FIREBASE_KEY),
                        null
                );
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOAD_ALL_EXCHANGE_RATES:
                while (data.moveToFirst()) {
                    mCurrencyValue = String.valueOf(data.getFloat(data.getColumnIndexOrThrow(ExchangeRateEntry.COL3_EXCHANGE_RATE)));
                    setExchangeRateInView();
                }
                return;
            case LOAD_ALL_USERS:
                getCurrentUser(data);
                return;
            case LOAD_SPECIFIC_PAYMENT:
                setPaymentInView(data);
                return;
            default:
                createLog("Whoops, something went wrong with the loader ID's");
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private class AsyncTaskExchangeRate extends AsyncTask<URL, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mCurrencySpinner.setEnabled(false);
            mCurrencySpinner.setClickable(false);
        }

        @Override
        protected String doInBackground(URL... urls) {
            try {
                String result = JsonHelper.makeHTTPRequest(urls[0]);
                mCurrencyValue = JsonHelper.getExchangeRate(result, mCurrencyName, getDefaultCurrency());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return mCurrencyValue;
        }

        @Override
        protected void onPostExecute(String aString) {
            mCurrencySpinner.setEnabled(true);
            mCurrencySpinner.setClickable(true);

            if (mCurrencyValue != null) {
                ContentValues values = new ContentValues();
                values.put(ExchangeRateEntry.COL1_APP_CURRENCY, getDefaultCurrency());
                values.put(ExchangeRateEntry.COL2_FOREIGN_CURRENCY, mCurrencyName);
                values.put(ExchangeRateEntry.COL3_EXCHANGE_RATE, mCurrencyValue);
                values.put(ExchangeRateEntry.COL4_TIME_CREATED, System.currentTimeMillis());

                Uri returnUri = getActivity().getContentResolver().insert(ExchangeRateEntry.CONTENT_URI_EXCHANGE_RATE, values);
                createLog("Exchange rate has been saved with uri: " + returnUri);
            }
            setExchangeRateInView();
        }
    }


    private void getCurrency(String urls) {

        if (CheckConnectivity.hasInternetConnection(getActivity())) {
            URL url = JsonHelper.createUrl(urls);

            AsyncTaskExchangeRate task = new AsyncTaskExchangeRate();
            task.execute(url);
        } else {
            Bundle bundle = new Bundle();
            bundle.putStringArray(Constants.EXCHANGE_RATE, new String[]{mCurrencyName});
            getActivity().getSupportLoaderManager().restartLoader(LOAD_ALL_EXCHANGE_RATES, bundle, AddPaymentDialog.this);
        }
    }
}


