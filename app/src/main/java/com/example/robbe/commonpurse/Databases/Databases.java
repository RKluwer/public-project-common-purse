package com.example.robbe.commonpurse.Databases;


public class Databases {

    private String key;
    private long timeCreated;
    private String name;
    private long lastAccessed;
    private String password;
    private String currency;

    public Databases() {
    }

    public Databases(long timeCreated, String name) {
        this.timeCreated = timeCreated;
        this.name = name;
    }

    public Databases(long timeCreated, String name, long lastAccessed) {
        this.timeCreated = timeCreated;
        this.name = name;
        this.lastAccessed = lastAccessed;
    }

    public Databases(long timeCreated, String name, long lastAccessed, String password) {
        this.timeCreated = timeCreated;
        this.name = name;
        this.lastAccessed = lastAccessed;

        if (password.length() > 3 && !name.equals(password)) {
            this.password = password;
        }
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        if (name.length() > 3){
            this.name = name;
        } else throw new Exception();
    }

    public long getLastAccessed() {
        return lastAccessed;
    }

    public void setPassword(String password) throws Exception {
        if (password.length() > 3) {
            this.password = password;
        } else throw new Exception();
    }

    public String getPassword(){
        return password;
    }

    public String getKey(){
        return key;
    }

    public void setKey (String key){
        this.key = key;
    }

    public String getCurrency() {return this.currency; }

    public void setCurrency(String currency){
        this.currency = currency;
    }

}
