package com.example.robbe.commonpurse.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.robbe.commonpurse.Fragments.FragmentContainer;
import com.example.robbe.commonpurse.Notes.AddNoteDialog;
import com.example.robbe.commonpurse.Notes.Note;
import com.example.robbe.commonpurse.Notes.NoteAdapter;
import com.example.robbe.commonpurse.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by robbe on 17-2-2018.
 */

public class NoteFragment extends Fragment {

    private static final String LOG_TAG = "NoteFragment";

    private View rootView;
    private ListView listView;
    private NoteAdapter noteAdapter;
    private ArrayList<Note> noteList;

    private FirebaseDatabase database;
    private DatabaseReference dbReference;
    private ChildEventListener childEventListener;

    private String databaseName;

    public NoteFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.note_fragment, container, false);

        FragmentContainer fc = (FragmentContainer) getActivity();
        databaseName = fc.getDatabaseName();

        database = FirebaseDatabase.getInstance();
        dbReference = database.getReference().child(databaseName).child(AddNoteDialog.NOTE_DB_NAME);
        listView = rootView.findViewById(R.id.note_list_view);

        noteList = new ArrayList<>();
        noteAdapter = new NoteAdapter(getActivity(), R.layout.note_list_item, noteList);
        listView.setAdapter(noteAdapter);

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Note note = dataSnapshot.getValue(Note.class);
                note.setId(dataSnapshot.getKey());
                Log.d(LOG_TAG, "Note list length: " + noteList.size());

                noteAdapter.insert(note, 0);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                String key = dataSnapshot.getKey();
                Note updatedNote = dataSnapshot.getValue(Note.class);

                for (Note un : noteList) {
                    if (key.equals(un.getId())) {
                        un.updateValues(updatedNote);
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                try {
                    for (Note un : noteList) {
                        if (key.equals(un.getId())) {
                            noteAdapter.remove(un);
                        }
                    }
                } catch (Exception e) {
                    Log.e(LOG_TAG, "An error has occurred");
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        dbReference.addChildEventListener(childEventListener);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                final Note chosenNote = noteAdapter.getItem(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("What would you like to do?");
                builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AddNoteDialog dialog = new AddNoteDialog();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("chosenNote", chosenNote);
                        dialog.setArguments(bundle);
                        dialog.show(getActivity().getFragmentManager(), "See Note Dialog");
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                }).setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dbReference.child(chosenNote.getId()).removeValue();
                    }
                });

                builder.show();
            }
        });

        return rootView;
    }
}
