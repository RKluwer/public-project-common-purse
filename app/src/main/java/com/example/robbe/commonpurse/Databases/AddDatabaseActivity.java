package com.example.robbe.commonpurse.Databases;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class AddDatabaseActivity extends AppCompatActivity {

    EditText addDatabaseEditText, passwordEditText;
    TextView duplicateWarningTv, shortPasswordWarning;
    DatabaseReference dbReference;

    ArrayList<Databases> allDatabases = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_only_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_only_save_button:
                saveDatabase();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_database_dialog);

        FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
        dbReference = fbDatabase.getReference().child(Constants.DATABASES);

        addDatabaseEditText = findViewById(R.id.add_database_name_edit_text);
        passwordEditText = findViewById(R.id.add_database_password_edit_text);
        duplicateWarningTv = findViewById(R.id.add_database_duplicate_warning);
        shortPasswordWarning = findViewById(R.id.add_database_short_password_warning);

        LinearLayout titleLayout = findViewById(R.id.add_database_title_ll);
        titleLayout.setVisibility(View.GONE);
        LinearLayout currencyLayout = findViewById(R.id.add_database_currency_layout);
        currencyLayout.setVisibility(View.VISIBLE);

        Spinner currencySpinner = findViewById(R.id.add_database_currency_spinner);
        ArrayAdapter spinnerAdapter = ArrayAdapter.createFromResource(
                AddDatabaseActivity.this,
                R.array.array_currencies,
                android.R.layout.simple_spinner_item);
        currencySpinner.setAdapter(spinnerAdapter);
        currencySpinner.setPrompt("Currency");
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        loadAllDatabases(dbReference);

        addDatabaseEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                duplicateWarningTv.setVisibility(View.GONE);
                for (Databases db: allDatabases){
                    if (db.getName().toLowerCase().equals(s.toString().trim().toLowerCase())){
                        duplicateWarningTv.setText(R.string.duplicate_value_exists);
                        duplicateWarningTv.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                shortPasswordWarning.setVisibility(View.GONE);
                if (s.length() < 7){
                    shortPasswordWarning.setText(R.string.too_short_password);
                    shortPasswordWarning.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void loadAllDatabases(DatabaseReference dbReference){
        dbReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Databases newDb = dataSnapshot.getValue(Databases.class);
                newDb.setKey(dataSnapshot.getKey());
                if (!allDatabases.contains(newDb)){
                    allDatabases.add(newDb);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                for (Databases db: allDatabases){
                    if (db.getKey().equals(dataSnapshot.getKey())){
                        allDatabases.remove(db);
                        allDatabases.add(dataSnapshot.getValue(Databases.class));
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                for (Databases db: allDatabases){
                    if (db.getKey().equals(dataSnapshot.getKey())){
                        allDatabases.remove(db);
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveDatabase() {
        if (duplicateWarningTv.getVisibility() == View.VISIBLE){
            Toast.makeText(this, "This database already exists. " +
                    "Please choose another names", Toast.LENGTH_SHORT).show();
            return;
        }

        if (shortPasswordWarning.getVisibility() == View.VISIBLE){
            Toast.makeText(this, "Your password is too short.", Toast.LENGTH_SHORT).show();
            return;
        }

        long timeCreated = System.currentTimeMillis();
        String newDatabaseName = addDatabaseEditText.getText().toString().trim();
        String newPassword = passwordEditText.getText().toString().trim();
        Databases db = new Databases(timeCreated, newDatabaseName, 0, newPassword);
        dbReference.push().setValue(db);

        SharedPreferences preferences = getSharedPreferences(Constants.SAVED_DATABASE, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SAVED_DATABASE, db.getName());
        editor.putString(Constants.SAVED_DATABASE_ID, String.valueOf(db.getTimeCreated()));
        editor.apply();

        Intent intent = new Intent(this, ChooseDatabaseToJoin.class);
        startActivity(intent);
    }
}
