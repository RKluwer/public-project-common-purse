package com.example.robbe.commonpurse.Notes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.robbe.commonpurse.Notes.Note;
import com.example.robbe.commonpurse.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by robbe on 18-2-2018.
 */

public class AddNoteDialog extends DialogFragment {

    public AddNoteDialog() {
    }

    public static final String NOTE_DB_NAME = "Notes";
    private static final String LOG_TAG = "AddNoteDialog";
    private static final String LUCIA = "Lucia";
    private static final String ROBBERT = "Robbert";
    private String databaseName;
    private String key;
    private Note existingNote = null;

    EditText titleEditText, amountEditText, noteEditText;
    Spinner currencySpinner;
    long time;

    private String currency, fileTitle, fileBodyText;
    private double amount;

    private FirebaseDatabase database;
    private DatabaseReference dbNoteReference;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(LOG_TAG, "OnCreateDialog called");

        Bundle bundle = getArguments();
        if (bundle != null) {
            existingNote = (Note) bundle.getSerializable("chosenNote");
            key = existingNote.getId();
            Log.d(LOG_TAG, "the child is: " + key);
        }
        return addNote();

    }

    private Dialog addNote() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.add_note_dialog, null);

        titleEditText = dialogView.findViewById(R.id.note_title_et);
        amountEditText = dialogView.findViewById(R.id.note_amount_et);
        noteEditText = dialogView.findViewById(R.id.note_text_et);
        currencySpinner = dialogView.findViewById(R.id.note_currency_spinner);

        database = FirebaseDatabase.getInstance();
        dbNoteReference = database.getReference().child(databaseName).child(NOTE_DB_NAME);

        builder.setView(dialogView).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (existingNote == null) {
                    saveNote();
                } else {
                    updateNote();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });


        currencySpinner = dialogView.findViewById(R.id.note_currency_spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.array_currencies, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(adapter);

        if (existingNote != null) {
            titleEditText.setText(existingNote.getTitle());
            amountEditText.setText(String.valueOf(existingNote.getAmount()));
            noteEditText.setText(existingNote.getBodyText());
            String currency = existingNote.getCurrency();

            int spinnerPosition = 0;
            switch (currency) {
                case "CZK":
                    spinnerPosition = 0;
                    currency = existingNote.getCurrency();
                    break;
                case "CHF":
                    spinnerPosition = 1;
                    currency = existingNote.getCurrency();
                    break;
                case "DKK":
                    spinnerPosition = 2;
                    currency = existingNote.getCurrency();
                    break;
                case "EUR":
                    spinnerPosition = 3;
                    currency = existingNote.getCurrency();
                    break;
                case "GBP":
                    spinnerPosition = 4;
                    currency = existingNote.getCurrency();
                    break;
                case "HUF":
                    spinnerPosition = 5;
                    currency = existingNote.getCurrency();
                    break;
                case "NOK":
                    spinnerPosition = 6;
                    currency = existingNote.getCurrency();
                    break;
                case "PLN":
                    spinnerPosition = 7;
                    currency = existingNote.getCurrency();
                    break;
                case "THB":
                    spinnerPosition = 8;
                    currency = existingNote.getCurrency();
                    break;
                case "USD":
                    spinnerPosition = 9;
                    currency = existingNote.getCurrency();
                    break;
                default:
                    spinnerPosition = 0;

            }
            currencySpinner.setSelection(spinnerPosition);
        }

        currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selection = (String) adapterView.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getResources().getString(R.string.CHF))) {
                        currency = getResources().getString(R.string.CHF);
                    } else if (selection.equals(getResources().getString(R.string.DKK))) {
                        currency = getResources().getString(R.string.DKK);
                    } else if (selection.equals(getResources().getString(R.string.EUR))) {
                        currency = getResources().getString(R.string.EUR);
                    } else if (selection.equals(getResources().getString(R.string.GBP))) {
                        currency = getResources().getString(R.string.GBP);
                    } else if (selection.equals(getResources().getString(R.string.HUF))) {
                        currency = getResources().getString(R.string.HUF);
                    } else if (selection.equals(getResources().getString(R.string.NOK))) {
                        currency = getResources().getString(R.string.NOK);
                    } else if (selection.equals(getResources().getString(R.string.PLN))) {
                        currency = getResources().getString(R.string.PLN);
                    } else if (selection.equals(getResources().getString(R.string.THB))) {
                        currency = getResources().getString(R.string.THB);
                    } else if (selection.equals(getResources().getString(R.string.USD))) {
                        currency = getResources().getString(R.string.USD);
                    } else {
                        currency = getResources().getString(R.string.CZK);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                currency = getResources().getString(R.string.CZK);
            }
        });

        return builder.create();
    }

    private void updateNote() {
        time = System.currentTimeMillis();

        Note updateNote = new Note(titleEditText.getText().toString(), existingNote.getUser(),
                Double.parseDouble(amountEditText.getText().toString()), currency,
                existingNote.getTimeCreated(), time, noteEditText.getText().toString());

        dbNoteReference.child(key).setValue(updateNote);

    }

    private void saveNote() {
        time = System.currentTimeMillis();
        fileTitle = titleEditText.getText().toString();
        fileBodyText = noteEditText.getText().toString();
        amount = Double.parseDouble(amountEditText.getText().toString());

        Note newNote = new Note(fileTitle, getUser(), amount, currency, time, time, fileBodyText);
        dbNoteReference.push().setValue(newNote);
    }

    private String getUser() {
        String user;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean isUserLucia = prefs.getBoolean("isUserLucia", false);

        if (isUserLucia) {
            user = LUCIA;
        } else {
            user = ROBBERT;
        }

        return user;
    }

    public void setDatabaseName(String databaseName){
        this.databaseName = databaseName;
    }
}
