package com.example.robbe.commonpurse.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import com.example.robbe.commonpurse.Constants.ExchangeRateEntry;

public class ExchangeRateDatabaseHelper extends SQLiteOpenHelper {

    private String tableName = null;
    public ExchangeRateDatabaseHelper(@Nullable Context context, @Nullable String name) {
        super(context, name, null, 1);
        tableName = name;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + tableName + " ("
                + ExchangeRateEntry.COL0_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ExchangeRateEntry.COL1_APP_CURRENCY + " TEXT NOT NULL, "
                + ExchangeRateEntry.COL2_FOREIGN_CURRENCY + " TEXT NOT NULL, "
                + ExchangeRateEntry.COL3_EXCHANGE_RATE + " INTEGER NOT NULL, "
                + ExchangeRateEntry.COL4_TIME_CREATED + " INTEGER NOT NULL)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF TABLE EXISTS " + tableName);
        onCreate(db);
    }
}
