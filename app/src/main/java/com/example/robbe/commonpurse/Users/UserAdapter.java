package com.example.robbe.commonpurse.Users;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.Constants.UserEntry;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

public class UserAdapter extends CursorAdapter {
    private boolean setViewUserDetails = false;

    public UserAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.user_list_item, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView nameTv = view.findViewById(R.id.user_list_item_name_tv);
        TextView timeCreatedTv = view.findViewById(R.id.user_list_item_time_created);
        TextView emailTv = view.findViewById(R.id.user_list_item_email_tv);
        ImageView profileImage = view.findViewById(R.id.user_list_item_profile_image);
        EditText amountEditText = view.findViewById(R.id.user_list_item_amount_edit_text);
        amountEditText.setVisibility(View.GONE);

        nameTv.setText(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COL4_FULLNAME)));
        emailTv.setText(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COL6_EMAIL)));

        DateFormat dateFormat = DateFormat.getDateTimeInstance();
        Date currentDate = new Date(cursor.getLong(cursor.getColumnIndexOrThrow(UserEntry.COL8_TIMECREATED)));
        timeCreatedTv.setText(dateFormat.format(currentDate));

        if (setViewUserDetails) {
            emailTv.setVisibility(View.VISIBLE);
            timeCreatedTv.setVisibility(View.VISIBLE);

        } else {
            emailTv.setVisibility(View.GONE);
            timeCreatedTv.setVisibility(View.GONE);
        }

        /*
        // Not yet applicable.
        if (currentUser.getUserAmount() != 0){
            amountEditText.setVisibility(View.VISIBLE);
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            String amount = decimalFormat.format(currentUser.getUserAmount());
            amountEditText.setText(amount);
        }
        */

        String photoId = cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COL9_PHOTOURI));

        if (photoId != null) {
            createLog("PhotoId saved");
            try {
                Glide.with(context)
                        .load(getTempFile(Uri.parse(photoId)))
                        .into(profileImage);
                createLog("Photo set from the internal storage");
            } catch (Exception e) {
                Glide.with(context)
                        .load(Uri.parse(photoId))
                        .into(profileImage);
            }
        } else {
            profileImage.setImageDrawable(context.getResources().getDrawable(R.drawable.smile));
        }

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_left);
        view.startAnimation(animation);
    }

    private String getFileName(File directory, String fileName) {
        File[] listFiles = directory.listFiles();
        try {
            for (File f : listFiles) {
                if (f.getName().contains(fileName)) {
                    return f.getName();
                }
            }
        } catch (NullPointerException e) {
            createLog("No such file found.");
        }
        return fileName;
    }

    private File getTempFile(Uri pictureUri) {
        File directory = Environment.getExternalStorageDirectory();
        File filePathSegment = new File(directory, pictureUri.getLastPathSegment());

        return new File(directory, getFileName(directory, filePathSegment.getName()));
    }

    private void createLog(String message) {
        Log.d("UserAdapter", message);
    }
}
