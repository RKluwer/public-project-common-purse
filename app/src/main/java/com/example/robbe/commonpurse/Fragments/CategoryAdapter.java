package com.example.robbe.commonpurse.Fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by robbe on 18-11-2017.
 */

public class CategoryAdapter extends FragmentPagerAdapter {

    private String databaseName;
    PaymentOverviewLeft paymentOverviewLeft;
    PaymentOverviewRight paymentOverviewRight;
    StandingsFragment standingsFragment;
    NoteFragment noteFragment;


    public CategoryAdapter(Context context, FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                standingsFragment = new StandingsFragment();
                standingsFragment.setDatabaseName(databaseName);
                return standingsFragment;
            case 1:
                paymentOverviewLeft = new PaymentOverviewLeft();
                return paymentOverviewLeft;
            case 2:
                paymentOverviewRight = new PaymentOverviewRight();
                return paymentOverviewRight;
            case 3:
                noteFragment = new NoteFragment();
                return noteFragment;
            default:
                return new StandingsFragment();
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Overview";
            case 1:
                return "Payments";
            case 2:
                return "Selected";
            case 3:
                return "Notes";
            default:
                return "Overview";
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public StandingsFragment getStandingsFragment() {
        return standingsFragment;
    }

    public PaymentOverviewLeft getPaymentOverviewLeft(){
        return paymentOverviewLeft;
    }

    public PaymentOverviewRight getPaymentOverviewRight() {
        return paymentOverviewRight;
    }

    public NoteFragment getNoteFragment(){
        return noteFragment;
    }

}
