package com.example.robbe.commonpurse.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.R;

import java.text.DecimalFormat;
import java.util.List;

public class UserStandingPaymentAdapter extends ArrayAdapter<Payment> {
    public UserStandingPaymentAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.user_standing_payment_list_item, parent, false);
        }

        TextView descriptionTv = listItemView.findViewById(R.id.user_standing_payment_description_tv);
        TextView amountTv = listItemView.findViewById(R.id.user_standing_payment_amount_tv);

        Payment currentPayment = getItem(position);

        if (currentPayment.getmCreator() == null){
            createLog("There is no creator for this payment");
            // do nothing
        } else if (currentPayment.getmCreator().equals(currentPayment.getUser())){
            String descriptionText = getContext().getResources().getString(R.string.you_owe)
                    + " "
                    + currentPayment.getReceiver()
                    + ":";
            descriptionTv.setText(descriptionText);
            descriptionTv.setTextColor(getContext().getResources().getColor(R.color.red));
            amountTv.setTextColor(descriptionTv.getCurrentTextColor());
        } else if (currentPayment.getmCreator().equals(currentPayment.getReceiver())){
            String descriptionText = getContext().getResources().getString(R.string.you_are_owed)
                    + " "
                    + currentPayment.getUser()
                    + ":";
            descriptionTv.setText(descriptionText);
            descriptionTv.setTextColor(getContext().getResources().getColor(R.color.green));
            amountTv.setTextColor(descriptionTv.getCurrentTextColor());
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String currency = preferences.getString("currency_pref", "EUR");

        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        String amount = decimalFormat.format(currentPayment.getAmount())
                + " "
                + currency;
        amountTv.setText(amount);

        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_left);
        listItemView.startAnimation(animation);

        return listItemView;
    }

    private void createLog (String message){
        Log.d("UserStandingPaymentAdap", message);
    }
}
