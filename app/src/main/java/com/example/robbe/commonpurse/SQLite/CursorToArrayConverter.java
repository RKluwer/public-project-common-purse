package com.example.robbe.commonpurse.SQLite;

import android.database.Cursor;

import com.example.robbe.commonpurse.Constants.PaymentEntry;
import com.example.robbe.commonpurse.Constants.UserEntry;
import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.Users.User;

import java.util.ArrayList;

public class CursorToArrayConverter {

    public static ArrayList<User> getUserArrayList(Cursor data){
        ArrayList<User> userList = new ArrayList<>();

        while (data.moveToNext()) {
            User newUser = new User();
            newUser.setFirebaseKey(data.getString(data.getColumnIndexOrThrow(UserEntry.COL1_FIREBASEID)));
            newUser.setFirstName(data.getString(data.getColumnIndexOrThrow(UserEntry.COL2_FIRSTNAME)));
            newUser.setLastName(data.getString(data.getColumnIndexOrThrow(UserEntry.COL3_LASTNAME)));
            newUser.setName(data.getString(data.getColumnIndexOrThrow(UserEntry.COL4_FULLNAME)));
            newUser.setId(data.getString(data.getColumnIndexOrThrow(UserEntry.COL5_USERID)));
            newUser.setEmailAddress(data.getString(data.getColumnIndexOrThrow(UserEntry.COL6_EMAIL)));
            newUser.setRole(data.getString(data.getColumnIndexOrThrow(UserEntry.COL7_USERROLE)));
            newUser.setTimeCreated(data.getLong(data.getColumnIndexOrThrow(UserEntry.COL8_TIMECREATED)));
            newUser.setPhotoId(data.getString(data.getColumnIndexOrThrow(UserEntry.COL9_PHOTOURI)));

            userList.add(newUser);
        }

        return userList;
    }

    public static User getUserFromCursor(Cursor data){
        User newUser = new User();
        newUser.setFirebaseKey(data.getString(data.getColumnIndexOrThrow(UserEntry.COL1_FIREBASEID)));
        newUser.setFirstName(data.getString(data.getColumnIndexOrThrow(UserEntry.COL2_FIRSTNAME)));
        newUser.setLastName(data.getString(data.getColumnIndexOrThrow(UserEntry.COL3_LASTNAME)));
        newUser.setName(data.getString(data.getColumnIndexOrThrow(UserEntry.COL4_FULLNAME)));
        newUser.setId(data.getString(data.getColumnIndexOrThrow(UserEntry.COL5_USERID)));
        newUser.setEmailAddress(data.getString(data.getColumnIndexOrThrow(UserEntry.COL6_EMAIL)));
        newUser.setRole(data.getString(data.getColumnIndexOrThrow(UserEntry.COL7_USERROLE)));
        newUser.setTimeCreated(data.getLong(data.getColumnIndexOrThrow(UserEntry.COL8_TIMECREATED)));
        newUser.setPhotoId(data.getString(data.getColumnIndexOrThrow(UserEntry.COL9_PHOTOURI)));

        return newUser;
    }

    public static ArrayList<Payment> getPaymentArrayList(Cursor data){
        ArrayList<Payment> allPayments = new ArrayList<>();
        while (data.moveToNext()) {
            Payment newPayment = new Payment();
            newPayment.setKey(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL1_FIREBASEID)));
            newPayment.setCurrency(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL2_CURRENCY)));
            newPayment.setUser(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL3_USER)));
            newPayment.setmCreator(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL4_CREATOR)));
            newPayment.setmCreatorRole(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL5_CREATOR_ROLE)));
            newPayment.setReceiver(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL6_RECEIVER)));
            newPayment.setAmount(data.getFloat(data.getColumnIndexOrThrow(PaymentEntry.COL7_AMOUNT_IN_CZK)));
            newPayment.setTotalAmount(data.getFloat(data.getColumnIndexOrThrow(PaymentEntry.COL8_TOTAL_AMOUNT)));
            newPayment.setmOriginalAmount(data.getFloat(data.getColumnIndexOrThrow(PaymentEntry.COL9_ORIGINAL_AMOUNT)));
            newPayment.setDescription(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL10_DESCRIPTION)));
            newPayment.setmTime(data.getLong(data.getColumnIndexOrThrow(PaymentEntry.COL11_TIME)));
            newPayment.setPhotoId(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL12_PHOTO_ID)));
            newPayment.setApproved(data.getInt(data.getColumnIndexOrThrow(PaymentEntry.COL13_APPROVED)) == 1);
            newPayment.setBasePayment(data.getInt(data.getColumnIndexOrThrow(PaymentEntry.COL14_BASEPAYMENT)) == 1);

            allPayments.add(newPayment);
        }

        return allPayments;
    }

    public static Payment getFirstPayment(Cursor data){
        Payment newPayment = new Payment();
        while (data.moveToFirst()) {
            newPayment.setKey(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL1_FIREBASEID)));
            newPayment.setCurrency(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL2_CURRENCY)));
            newPayment.setUser(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL3_USER)));
            newPayment.setmCreator(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL4_CREATOR)));
            newPayment.setmCreatorRole(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL5_CREATOR_ROLE)));
            newPayment.setReceiver(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL6_RECEIVER)));
            newPayment.setAmount(data.getFloat(data.getColumnIndexOrThrow(PaymentEntry.COL7_AMOUNT_IN_CZK)));
            newPayment.setTotalAmount(data.getFloat(data.getColumnIndexOrThrow(PaymentEntry.COL8_TOTAL_AMOUNT)));
            newPayment.setmOriginalAmount(data.getFloat(data.getColumnIndexOrThrow(PaymentEntry.COL9_ORIGINAL_AMOUNT)));
            newPayment.setDescription(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL10_DESCRIPTION)));
            newPayment.setmTime(data.getLong(data.getColumnIndexOrThrow(PaymentEntry.COL11_TIME)));
            newPayment.setPhotoId(data.getString(data.getColumnIndexOrThrow(PaymentEntry.COL12_PHOTO_ID)));
            newPayment.setApproved(data.getInt(data.getColumnIndexOrThrow(PaymentEntry.COL13_APPROVED)) == 1);
            newPayment.setBasePayment(data.getInt(data.getColumnIndexOrThrow(PaymentEntry.COL14_BASEPAYMENT)) == 1);
        }

        return newPayment;
    }
}
