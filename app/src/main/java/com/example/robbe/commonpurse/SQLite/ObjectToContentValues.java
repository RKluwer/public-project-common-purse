package com.example.robbe.commonpurse.SQLite;

import android.content.ContentValues;

import com.example.robbe.commonpurse.Constants.PaymentEntry;
import com.example.robbe.commonpurse.Constants.UserEntry;
import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.Users.User;

public class ObjectToContentValues {

    public static ContentValues getPaymentContentValues(Payment payment, int isOnline) {
        ContentValues values = new ContentValues();

        values.put(PaymentEntry.COL1_FIREBASEID, payment.getKey());
        values.put(PaymentEntry.COL2_CURRENCY, payment.getCurrency());
        values.put(PaymentEntry.COL3_USER, payment.getUser());
        values.put(PaymentEntry.COL4_CREATOR, payment.getmCreator());
        values.put(PaymentEntry.COL5_CREATOR_ROLE, payment.getmCreatorRole());
        values.put(PaymentEntry.COL6_RECEIVER, payment.getReceiver());
        values.put(PaymentEntry.COL7_AMOUNT_IN_CZK, payment.getAmount());
        values.put(PaymentEntry.COL8_TOTAL_AMOUNT, payment.getTotalAmount());
        values.put(PaymentEntry.COL9_ORIGINAL_AMOUNT, payment.getmOriginalAmount());
        values.put(PaymentEntry.COL10_DESCRIPTION, payment.getDescription());
        values.put(PaymentEntry.COL11_TIME, payment.getmTime());
        values.put(PaymentEntry.COL12_PHOTO_ID, payment.getPhotoId());
        values.put(PaymentEntry.COL13_APPROVED, payment.isApproved() ? Constants.TRUE : Constants.FALSE);
        values.put(PaymentEntry.COL14_BASEPAYMENT, payment.isBasePayment() ? Constants.TRUE : Constants.FALSE);
        values.put(PaymentEntry.COL15_ISPOSTED, isOnline);

        return values;
    }

    public static ContentValues getUserContentValues(User user) {
        ContentValues values = new ContentValues();

        values.put(UserEntry.COL1_FIREBASEID, user.getFirebaseKey());
        values.put(UserEntry.COL2_FIRSTNAME, user.getFirstName());
        values.put(UserEntry.COL3_LASTNAME, user.getLastName());
        values.put(UserEntry.COL4_FULLNAME, user.getName());
        values.put(UserEntry.COL5_USERID, user.getId());
        values.put(UserEntry.COL6_EMAIL, user.getEmailAddress());
        values.put(UserEntry.COL7_USERROLE, user.getRole());
        values.put(UserEntry.COL8_TIMECREATED, user.getTimeCreated());
        values.put(UserEntry.COL9_PHOTOURI, user.getPhotoId());

        return values;
    }
}
