package com.example.robbe.commonpurse.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.Users.User;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class UserStandingAdapter extends ArrayAdapter<User> {
    List<Payment> paymentList = new ArrayList<>();
    ArrayList<Payment> selectedPaymentList = new ArrayList<>();

    public UserStandingAdapter(@NonNull Context context, int resource, @NonNull List<User> objects) {
        super(context, resource, objects);
    }

    public UserStandingAdapter(@NonNull Context context, int resource, @NonNull List<User> objects, List<Payment> paymentList) {
        super(context, resource, objects);
        this.paymentList = paymentList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.user_staning_list_item, parent, false);
        }

        TextView userNameTv = listItemView.findViewById(R.id.user_standing_name);
        TextView amountTv = listItemView.findViewById(R.id.user_standing_amount);
        TextView amountDescriptionTv = listItemView.findViewById(R.id.user_standing_amount_description_tv);

        // Setting the visibility default to true
        amountDescriptionTv.setVisibility(View.VISIBLE);
        amountTv.setVisibility(View.VISIBLE);

        final User currentUser = getItem(position);

        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String currency = preferences.getString("currency_pref", "EUR");

        try {
            float flAmountValue = currentUser.getUserAmount() < 0 ? currentUser.getUserAmount() * -1 : currentUser.getUserAmount();
            String amount = decimalFormat.format(flAmountValue) + currency;
            userNameTv.setText(currentUser.getName());
            amountTv.setText(amount);

            if (currentUser.getUserAmount() > 0.00){
                amountDescriptionTv.setText(getContext().getResources().getString(R.string.amount_is_positive));
            } else if (currentUser.getUserAmount() < 0.00){
                amountDescriptionTv.setText(getContext().getResources().getString(R.string.amount_is_negative));
            } else {
                amountDescriptionTv.setText(getContext().getResources().getString(R.string.amount_is_zero));
                amountTv.setVisibility(View.GONE);
            }
        } catch (NullPointerException e){
            Toast.makeText(getContext(), "Something has gone wrong", Toast.LENGTH_SHORT).show();
        }

        if (currentUser.getUserAmount() < 0){
            userNameTv.setTextColor(getContext().getResources().getColor(R.color.red));
            amountTv.setTextColor(getContext().getResources().getColor(R.color.red));
            amountDescriptionTv.setTextColor(getContext().getResources().getColor(R.color.red));
        } else {
            userNameTv.setTextColor(getContext().getResources().getColor(R.color.green));
            amountTv.setTextColor(getContext().getResources().getColor(R.color.green));
            amountDescriptionTv.setTextColor(getContext().getResources().getColor(R.color.green));
        }

        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_left);
        listItemView.startAnimation(animation);

        return listItemView;
    }

    private void createLog (String message){
        Log.d("UserStandingAdapter", message);
    }
}
