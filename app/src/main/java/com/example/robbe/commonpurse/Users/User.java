package com.example.robbe.commonpurse.Users;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.Serializable;

public class User implements Serializable {
    public User() {
    }

    private String firebaseKey;
    private String firstName;
    private String lastName;
    private String name;
    private String id;
    private String emailAddress;
    private String role;
    private long timeCreated;
    private boolean isActive;
    private float amount;
    private String photoId;
    private String photoUri;

    public User(String name, String id, String emailAddress, String role, long timeCreated) {
        this.name = name;
        this.id = id;
        this.emailAddress = emailAddress;
        this.role = role;
        this.timeCreated = timeCreated;
    }

    public User(String name, String id, String emailAddress, String role, long timeCreated, boolean isActive) {
        this.name = name;
        this.id = id;
        this.emailAddress = emailAddress;
        this.role = role;
        this.timeCreated = timeCreated;
        this.isActive = isActive;
    }

    public User(String name, String id, String emailAddress, String role, long timeCreated, float amount) {
        this.name = name;
        this.id = id;
        this.emailAddress = emailAddress;
        this.role = role;
        this.timeCreated = timeCreated;
        this.amount = amount;
    }

    public String getFirebaseKey(){ return this.firebaseKey;}

    public void setFirebaseKey(String firebaseKey) {this.firebaseKey = firebaseKey; }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri (String photoUri){
        this.photoUri = photoUri;
    }

    public String getFirstName() {return firstName; }

    public String getLastName() {return lastName; }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public void setEmailAddress(String emailAddress){this.emailAddress = emailAddress; }

    public void setRole(String role){this.role = role; }

    public void setTimeCreated(long timeCreated){this.timeCreated = timeCreated; }

    public void setId(String id){this.id = id; }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getRole() {
        return role;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public float getUserAmount() {
        return amount;
    }

    public void setUserAmount(float amount) {
        this.amount = amount;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPhotoId(String photoId){
        this.photoId = photoId;
    }

    public String getPhotoId(){
        return this.photoId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: " + this.name)
                .append(System.getProperty("line.separator"))
                .append("Email: " + this.emailAddress)
                .append(System.getProperty("line.separator"))
                .append("Time created: " + this.timeCreated)
                .append(System.getProperty("line.separator"))
                .append("Is inactive: " + this.isActive)
                .append(System.getProperty("line.separator"))
                .append("Role: " + this.role);
        return sb.toString();
    }
}
