package com.example.robbe.commonpurse.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by robbe on 17-2-2018.
 */

public class OverviewFragment extends Fragment {

    private static final String LOG_TAG = "OverviewFragment";
    private static final String OVERALLTOTALS = "overallTotals";
    private static final String OVERALLFOROTHER = "overallForOther";
    private static final String TOTALS = "totals";
    private static final String MONTHS = "monts";
    private static final String WEEKS = "weeks";

    TextView totalInputTv, totalInputLucia, totalInputRobbert, averageInputTv, averageInputLucia, averageInputRobbert,
            thisMonthTv, thisMonthLucia, thisMonthRobbert, averageMonthTv, averageMonthLucia, averageMonthRobbert, thisWeekTv, thisWeekLucia,
            thisWeekRobbert, averageWeekTv, averageWeekLucia, averageWeekRobbert, currentStandingLucia,
            currentStandingRobbert, youOweTv, youOweLucia, youOweRobbert, percentageForOtherTv, percentageForOtherLuciaTv,
            percentageForOtherRobbertTv, averageInputTotalTv, averageInputTotalLucia, averageInputTotalRobbert,
            thisMonthTotalLucia, thisMonthTotalTv, thisMontTotalRobbert, thisWeekTotalTv, thisWeekTotalLucia, thisWeekTotalRobbert,
            averageWeekTotalTv, averageWeekTotalLucia, averageWeekTotalRobbert, averageMonthTotalTv, averageMonthTotalLucia,
            averageMontTotalRobbert;

    ImageView optionsButton;

    Switch totalSwitch, forOtherSwitch, overallTotalsSwitch, monthTotalSwitch, weekTotalSwitch;

    GridLayout gridLayout;
    LinearLayout optionLinearLayoutParent, optionsLinearLayoutButtons;
    RelativeLayout mainLinearLayout;

    ProgressBar progressBar;

    Payment payment;
    boolean user;

    double monthTotalLuciaForOther[] = new double[12];
    double monthTotalLucia[] = new double[12];

    double weekTotalLuciaForOther[] = new double[52];
    double weekTotalLucia[] = new double[52];

    double monthTotalRobbertForOther[] = new double[12];
    double monthTotalRobbert[] = new double[12];

    double weekTotalRobbertForOther[] = new double[52];
    double weekTotalRobbert[] = new double[52];

    Calendar calendar;
    Date date;
    int todaysWeek = 0;
    int todaysMonth = 0;
    int todaysYear = 0;

    float robWeekTotalForOther = 0;
    float lucWeekTotalForOther = 0;

    float robWeekTotal = 0;
    float lucWeekTotal = 0;

    float robMonthTotalForOther = 0;
    float lucMonthTotalForOther = 0;

    float robMonthTotal = 0;
    float lucMonthTotal = 0;

    float lucTotal = 0;
    float robTotal = 0;

    float lucInputTotal = 0;
    float robInputTotal = 0;

    int lucCount = 0;
    int robCount = 0;
    private boolean mUser;

    private Bundle savedState = null;

    private boolean isOverallTotalsViewChecked, isForOtherViewChecked, isTotalsChecked,
            isMontsChecked, isWeeksChecked;

    View rootView;
    FirebaseDatabase database;
    DatabaseReference dBReferenceLucia;
    DatabaseReference dBReferenceLuciaBackup;
    DatabaseReference dBReferenceRobbert;
    DatabaseReference dBReferenceRobbertBackup;
    ChildEventListener childEventListenerLucia;
    ChildEventListener childEventListenerRobbert;

    TextView acceptPaymentMainText, acceptPaymentTitle;

    public OverviewFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resetNumbers();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.overview_fragment, container, false);

        calendar = Calendar.getInstance();
        database = FirebaseDatabase.getInstance();
        dBReferenceLucia = database.getReference().child("Lucia");
        dBReferenceLuciaBackup = database.getReference().child("LuciaBackup");
        dBReferenceRobbert = database.getReference().child("Robbert");
        dBReferenceRobbertBackup = database.getReference().child("RobbertBackup");

        // Total input views
        totalInputTv = rootView.findViewById(R.id.total_input_tv);
        totalInputLucia = rootView.findViewById(R.id.tv_total_input_lucia);
        totalInputRobbert = rootView.findViewById(R.id.tv_total_input_robbert);
        averageInputTv = rootView.findViewById(R.id.average_input_tv);
        averageInputLucia = rootView.findViewById(R.id.tv_average_input_lucia);
        averageInputRobbert = rootView.findViewById(R.id.tv_average_input_robbert);
        youOweTv = rootView.findViewById(R.id.you_owe_tv);
        youOweLucia = rootView.findViewById(R.id.tv_you_owe_lucia);
        youOweRobbert = rootView.findViewById(R.id.tv_you_owe_robbert);
        percentageForOtherTv = rootView.findViewById(R.id.percentage_for_other_tv);
        percentageForOtherLuciaTv = rootView.findViewById(R.id.tv_percentage_for_other_lucia);
        percentageForOtherRobbertTv = rootView.findViewById(R.id.tv_percentage_for_other_robbert);
        averageInputTotalTv = rootView.findViewById(R.id.average_input_total_tv);
        averageInputTotalLucia = rootView.findViewById(R.id.tv_average_input_total_lucia);
        averageInputTotalRobbert = rootView.findViewById(R.id.tv_average_input_total_robbert);
        currentStandingLucia = rootView.findViewById(R.id.tv_current_standing_lucia);
        currentStandingRobbert = rootView.findViewById(R.id.tv_current_standing_robbert);

        // This month views
        thisMonthTv = rootView.findViewById(R.id.this_month_tv);
        thisMonthLucia = rootView.findViewById(R.id.tv_month_lucia);
        thisMonthRobbert = rootView.findViewById(R.id.tv_month_robbert);
        averageMonthTv = rootView.findViewById(R.id.average_month_tv);
        averageMonthRobbert = rootView.findViewById(R.id.tv_average_month_robbert);
        averageMonthLucia = rootView.findViewById(R.id.tv_average_month_lucia);
        thisMonthTotalTv = rootView.findViewById(R.id.this_month_total_tv);
        thisMonthTotalLucia = rootView.findViewById(R.id.tv_month_total_lucia);
        thisMontTotalRobbert = rootView.findViewById(R.id.tv_month_total_robbert);
        averageMonthTotalTv = rootView.findViewById(R.id.average_month_total_tv);
        averageMonthTotalLucia = rootView.findViewById(R.id.tv_average_month_total_lucia);
        averageMontTotalRobbert = rootView.findViewById(R.id.tv_average_month_total_robbert);

        // This week views
        thisWeekTv = rootView.findViewById(R.id.this_week_tv);
        thisWeekLucia = rootView.findViewById(R.id.tv_week_lucia);
        thisWeekRobbert = rootView.findViewById(R.id.tv_week_robbert);
        averageWeekTv = rootView.findViewById(R.id.average_week_tv);
        averageWeekLucia = rootView.findViewById(R.id.tv_average_week_lucia);
        averageWeekRobbert = rootView.findViewById(R.id.tv_average_week_robbert);
        thisWeekTotalTv = rootView.findViewById(R.id.this_week_total_tv);
        thisWeekTotalLucia = rootView.findViewById(R.id.tv_week_total_lucia);
        thisWeekTotalRobbert = rootView.findViewById(R.id.tv_week_total_robbert);
        averageWeekTotalTv = rootView.findViewById(R.id.average_week_total_tv);
        averageWeekTotalLucia = rootView.findViewById(R.id.tv_average_week_total_lucia);
        averageWeekTotalRobbert = rootView.findViewById(R.id.tv_average_week_total_robbert);

        //Main Layouts
        mainLinearLayout = rootView.findViewById(R.id.overview_linear_main);
        optionsLinearLayoutButtons = rootView.findViewById(R.id.overview_options_button_layout);
        optionLinearLayoutParent = rootView.findViewById(R.id.overview_option_parent_layout);
        gridLayout = rootView.findViewById(R.id.overview_grid_layout);
        progressBar = rootView.findViewById(R.id.overview_progressBar);

        //Switches
        totalSwitch = rootView.findViewById(R.id.overview_switch_totals);
        forOtherSwitch = rootView.findViewById(R.id.overview_switch_other_amount);
        overallTotalsSwitch = rootView.findViewById(R.id.overview_switch_total);
        monthTotalSwitch = rootView.findViewById(R.id.overview_switch_total_month);
        weekTotalSwitch = rootView.findViewById(R.id.overview_switch_total_week);
        optionsButton = rootView.findViewById(R.id.overview_close_options);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        user = preferences.getBoolean("isUserLucia", false);

        getUser();
        getLuciaData();
        getRobbertData();




        /**
         * Below are the switches that determine what analyses are visible.
         * @overallTotalSwitch decides whether the user wants to see the total amount he/she puts in.
         * @forOtherSwitch decides whether the user wants to see the amount for the other person
         * he/she puts in
         * @totalSwitch decides whether a overview of the total inputs is shown.
         * @montTotalSwitch decides whether the totals per month as shown
         * @weekTotalSwitch decides whether the week totals are shown.
         */

        overallTotalsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isOverallTotalsViewChecked = setOverallTotalsVisibility(isChecked);
            }
        });

        forOtherSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isForOtherViewChecked = setForOtherVisibility(isChecked);
            }
        });

        totalSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isTotalsChecked = setTotalVisibility(isChecked);
            }
        });

        monthTotalSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isMontsChecked = setMonthTotalVisibility(isChecked);
            }
        });

        weekTotalSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isWeeksChecked = setWeekTotalVisibility(isChecked);
            }
        });


        /**
         * @optionsLinearLayout buttons is the linear layout that expands the options buttons
         * @optionsButton is the button that makes the options buttons disappear and is visible
         * in the options buttons layout.
         */
        optionsLinearLayoutButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionLinearLayoutParent.setVisibility(View.VISIBLE);
                optionLinearLayoutParent.setLayoutAnimationListener(null);
                optionLinearLayoutParent.setAlpha(0.0f);
                optionLinearLayoutParent
                        .animate()
                        .setDuration(500)
                        .alpha(1.0f)
                        .start();
            }
        });

        optionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionLinearLayoutParent
                        .animate()
                        .setDuration(500)
                        .alpha(0.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                if(optionLinearLayoutParent.getAlpha() == 0.0f) {
                                    optionLinearLayoutParent.setVisibility(View.GONE);
                                    optionLinearLayoutParent.animate().setListener(null);
                                }

                            }
                        })
                ;
            }
        });

        return rootView;
    }

    private void getUser() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUser = prefs.getBoolean("isUserLucia", false);
    }

    private void getLuciaData() {
        Date todaysDate = new Date(System.currentTimeMillis());
        calendar.setTime(todaysDate);
        todaysWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        todaysMonth = calendar.get(Calendar.MONTH);
        todaysYear = calendar.get(Calendar.YEAR);
        Log.d(LOG_TAG, "Todays week: " + todaysWeek + " Todays month: " + todaysMonth + " Todays year: " + todaysYear);

        childEventListenerLucia = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                payment = dataSnapshot.getValue(Payment.class);
                payment.setKey(dataSnapshot.getKey());
                if (payment.getmCreator().equals(payment.getUser())) {
                    lucTotal += payment.getAmount();
                    lucInputTotal += payment.getTotalAmount();
                    lucCount++;

                    date = new Date(payment.getmTime());
                    calendar.setTime(date);
                    int week = calendar.get(Calendar.WEEK_OF_YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int year = calendar.get(Calendar.YEAR);

                    if (todaysWeek == week && todaysYear == year && payment.getAmount() < 15000) {
                        lucWeekTotalForOther += payment.getAmount();
                        lucWeekTotal += payment.getTotalAmount();
                    }
                    if (todaysMonth == month && todaysYear == year && payment.getAmount() < 15000) {
                        lucMonthTotalForOther += payment.getAmount();
                        lucMonthTotal += payment.getTotalAmount();
                    }

                    if ((todaysYear == year || (todaysMonth < month && todaysYear == (year + 1))) && payment.getAmount() < 15000) {
                        monthTotalLuciaForOther[month] += payment.getAmount();
                        monthTotalLucia[month] += payment.getTotalAmount();
                        weekTotalLuciaForOther[week - 1] += payment.getAmount();
                        weekTotalLucia[week - 1] += payment.getTotalAmount();
                    }
                    setData();
                } else if (mUser) {
                    AlertDialog.Builder acceptPaymentDialog = new AlertDialog.Builder(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.accept_payment_dialog, null);

                    acceptPaymentMainText = view.findViewById(R.id.accept_payment_dialog_main_text);
                    acceptPaymentTitle = view.findViewById(R.id.accept_payment_dialog_title);

                    acceptPaymentDialog.setView(view);
                    acceptPaymentTitle.setText(getActivity().getResources().getString(R.string.accept_this_payment));
                    String paymentText = String.format("Payment amount: " + payment.getAmount() + "%n" + "Entered by: " + payment.getmCreator() + "%n" + "For: " + payment.getDescription());
                    acceptPaymentMainText.setText(paymentText);

                    acceptPaymentDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            payment.setmCreator(payment.getUser());
                            dBReferenceLucia.child(payment.getKey()).setValue(payment);
                        }
                    });

                    acceptPaymentDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dBReferenceLucia.child(payment.getKey()).removeValue();
                        }
                    });

                    acceptPaymentDialog.show();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        dBReferenceLucia.addChildEventListener(childEventListenerLucia);
    }

    private void getRobbertData() {
        childEventListenerRobbert = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                payment = dataSnapshot.getValue(Payment.class);
                payment.setKey(dataSnapshot.getKey());
                if (payment.getmCreator().equals(payment.getUser())) {
                    robTotal += payment.getAmount();
                    robInputTotal += payment.getTotalAmount();
                    robCount++;

                    date = new Date(payment.getmTime());
                    calendar.setTime(date);
                    int week = calendar.get(Calendar.WEEK_OF_YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int year = calendar.get(Calendar.YEAR);

                    if (todaysWeek == week && todaysYear == year && payment.getAmount() < 15000) {
                        robWeekTotalForOther += payment.getAmount();
                        robWeekTotal += payment.getTotalAmount();
                    }
                    if (todaysMonth == month && todaysYear == year && payment.getAmount() < 15000) {
                        robMonthTotalForOther += payment.getAmount();
                        robMonthTotal += payment.getTotalAmount();
                    }

                    if ((todaysYear == year || (todaysMonth < month && todaysYear == (year + 1))) && payment.getAmount() < 15000) {
                        monthTotalRobbertForOther[month] += payment.getAmount();
                        monthTotalRobbert[month] += payment.getTotalAmount();
                        weekTotalRobbertForOther[week - 1] += payment.getAmount();
                        weekTotalRobbert[week - 1] += payment.getTotalAmount();
                    }
                } else if (!mUser) {
                    AlertDialog.Builder acceptPaymentDialog = new AlertDialog.Builder(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.accept_payment_dialog, null);
                    acceptPaymentTitle = view.findViewById(R.id.accept_payment_dialog_title);
                    acceptPaymentMainText = view.findViewById(R.id.accept_payment_dialog_main_text);

                    acceptPaymentDialog.setView(view);
                    acceptPaymentTitle.setText(getActivity().getResources().getString(R.string.accept_this_payment));
                    String paymentText = String.format("Payment amount: " + payment.getAmount()
                            + "%n" + "Entered by: " + payment.getmCreator() + "%n" + "For: " + payment.getDescription());
                    acceptPaymentMainText.setText(paymentText);

                    acceptPaymentDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            payment.setmCreator(payment.getUser());
                            dBReferenceRobbert.child(payment.getKey()).setValue(payment);
                            Log.d(LOG_TAG, "Payment successfully updated");
                        }
                    });

                    acceptPaymentDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dBReferenceRobbert.child(payment.getKey()).removeValue();
                            Log.d(LOG_TAG, "Payment successfully removed");
                        }
                    });

                    acceptPaymentDialog.show();
                    Log.d(LOG_TAG, "Dialog shown");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        dBReferenceRobbert.addChildEventListener(childEventListenerRobbert);

        dBReferenceRobbert.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                setBoldText();
                setData();
                getAveragesLucia();
                getAveragesRobbert();
                setTextVisibility();
                Log.d(LOG_TAG, "On data changed is called, averages are calculated");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setTextVisibility() {
        gridLayout.setVisibility(View.VISIBLE);
        ScrollView.LayoutParams params = new ScrollView.LayoutParams
                (this.rootView.getWidth(), this.rootView.getHeight());
        mainLinearLayout.setLayoutParams(params);
        optionsLinearLayoutButtons.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private boolean setOverallTotalsVisibility(boolean isChecked) {
        if (isChecked) {
            // Totals

            totalInputTv.setVisibility(View.GONE);
            totalInputLucia.setVisibility(View.GONE);
            totalInputRobbert.setVisibility(View.GONE);
            averageInputTotalTv.setVisibility(View.GONE);
            averageInputTotalLucia.setVisibility(View.GONE);
            averageInputTotalRobbert.setVisibility(View.GONE);


            // Months

            averageMonthTotalTv.setVisibility(View.GONE);
            averageMonthTotalLucia.setVisibility(View.GONE);
            averageMontTotalRobbert.setVisibility(View.GONE);
            thisMonthTotalTv.setVisibility(View.GONE);
            thisMonthTotalLucia.setVisibility(View.GONE);
            thisMontTotalRobbert.setVisibility(View.GONE);


            // Weeks

            averageWeekTotalTv.setVisibility(View.GONE);
            averageWeekTotalLucia.setVisibility(View.GONE);
            averageWeekTotalRobbert.setVisibility(View.GONE);
            thisWeekTotalTv.setVisibility(View.GONE);
            thisWeekTotalLucia.setVisibility(View.GONE);
            thisWeekTotalRobbert.setVisibility(View.GONE);

            return true;
        } else {
            // Totals
            if (!isTotalsChecked) {
                totalInputTv.setVisibility(View.VISIBLE);
                totalInputLucia.setVisibility(View.VISIBLE);
                totalInputRobbert.setVisibility(View.VISIBLE);
                averageInputTotalTv.setVisibility(View.VISIBLE);
                averageInputTotalLucia.setVisibility(View.VISIBLE);
                averageInputTotalRobbert.setVisibility(View.VISIBLE);
            }

            // Months
            if (!isMontsChecked) {
                averageMonthTotalTv.setVisibility(View.VISIBLE);
                averageMonthTotalLucia.setVisibility(View.VISIBLE);
                averageMontTotalRobbert.setVisibility(View.VISIBLE);
                thisMonthTotalTv.setVisibility(View.VISIBLE);
                thisMonthTotalLucia.setVisibility(View.VISIBLE);
                thisMontTotalRobbert.setVisibility(View.VISIBLE);
            }

            // Weeks
            if (!isWeeksChecked) {
                averageWeekTotalTv.setVisibility(View.VISIBLE);
                averageWeekTotalLucia.setVisibility(View.VISIBLE);
                averageWeekTotalRobbert.setVisibility(View.VISIBLE);
                thisWeekTotalTv.setVisibility(View.VISIBLE);
                thisWeekTotalLucia.setVisibility(View.VISIBLE);
                thisWeekTotalRobbert.setVisibility(View.VISIBLE);
            }
            return false;
        }
    }

    private boolean setForOtherVisibility(boolean isChecked) {
        if (isChecked) {
            // Totals
            youOweTv.setVisibility(View.GONE);
            youOweLucia.setVisibility(View.GONE);
            youOweRobbert.setVisibility(View.GONE);
            percentageForOtherTv.setVisibility(View.GONE);
            percentageForOtherLuciaTv.setVisibility(View.GONE);
            percentageForOtherRobbertTv.setVisibility(View.GONE);

            // Months
            averageMonthTv.setVisibility(View.GONE);
            averageMonthLucia.setVisibility(View.GONE);
            averageMonthRobbert.setVisibility(View.GONE);
            thisMonthTv.setVisibility(View.GONE);
            thisMonthLucia.setVisibility(View.GONE);
            thisMonthRobbert.setVisibility(View.GONE);

            //Weeks
            averageWeekTv.setVisibility(View.GONE);
            averageWeekLucia.setVisibility(View.GONE);
            averageWeekRobbert.setVisibility(View.GONE);
            thisWeekTv.setVisibility(View.GONE);
            thisWeekLucia.setVisibility(View.GONE);
            thisWeekRobbert.setVisibility(View.GONE);
            return true;
        } else {
            // Totals
            if (!isTotalsChecked) {
                youOweTv.setVisibility(View.VISIBLE);
                youOweLucia.setVisibility(View.VISIBLE);
                youOweRobbert.setVisibility(View.VISIBLE);
                percentageForOtherTv.setVisibility(View.VISIBLE);
                percentageForOtherLuciaTv.setVisibility(View.VISIBLE);
                percentageForOtherRobbertTv.setVisibility(View.VISIBLE);
            }

            // Months
            if (!isMontsChecked) {
                averageMonthTv.setVisibility(View.VISIBLE);
                averageMonthLucia.setVisibility(View.VISIBLE);
                averageMonthRobbert.setVisibility(View.VISIBLE);
                thisMonthTv.setVisibility(View.VISIBLE);
                thisMonthLucia.setVisibility(View.VISIBLE);
                thisMonthRobbert.setVisibility(View.VISIBLE);
            }

            //Weeks
            if (!isWeeksChecked) {
                averageWeekTv.setVisibility(View.VISIBLE);
                averageWeekLucia.setVisibility(View.VISIBLE);
                averageWeekRobbert.setVisibility(View.VISIBLE);
                thisWeekTv.setVisibility(View.VISIBLE);
                thisWeekLucia.setVisibility(View.VISIBLE);
                thisWeekRobbert.setVisibility(View.VISIBLE);
            }
            return false;
        }
    }

    private boolean setTotalVisibility(boolean isChecked) {
        if (isChecked && !isOverallTotalsViewChecked) {
            totalInputTv.setVisibility(View.GONE);
            totalInputLucia.setVisibility(View.GONE);
            totalInputRobbert.setVisibility(View.GONE);
            averageInputTotalTv.setVisibility(View.GONE);
            averageInputTotalLucia.setVisibility(View.GONE);
            averageInputTotalRobbert.setVisibility(View.GONE);
        }
        if (isChecked && !isForOtherViewChecked) {
            youOweTv.setVisibility(View.GONE);
            youOweLucia.setVisibility(View.GONE);
            youOweRobbert.setVisibility(View.GONE);
            percentageForOtherTv.setVisibility(View.GONE);
            percentageForOtherLuciaTv.setVisibility(View.GONE);
            percentageForOtherRobbertTv.setVisibility(View.GONE);
        }
        if (!isChecked && !isOverallTotalsViewChecked) {
            totalInputTv.setVisibility(View.VISIBLE);
            totalInputLucia.setVisibility(View.VISIBLE);
            totalInputRobbert.setVisibility(View.VISIBLE);
            averageInputTotalTv.setVisibility(View.VISIBLE);
            averageInputTotalLucia.setVisibility(View.VISIBLE);
            averageInputTotalRobbert.setVisibility(View.VISIBLE);
        }
        if (!isChecked && !isForOtherViewChecked) {
            youOweTv.setVisibility(View.VISIBLE);
            youOweLucia.setVisibility(View.VISIBLE);
            youOweRobbert.setVisibility(View.VISIBLE);
            percentageForOtherTv.setVisibility(View.VISIBLE);
            percentageForOtherLuciaTv.setVisibility(View.VISIBLE);
            percentageForOtherRobbertTv.setVisibility(View.VISIBLE);
        }
        return isChecked;
    }

    private boolean setMonthTotalVisibility(boolean isChecked) {
        if (isChecked && !isOverallTotalsViewChecked) {
            averageMonthTotalTv.setVisibility(View.GONE);
            averageMonthTotalLucia.setVisibility(View.GONE);
            averageMontTotalRobbert.setVisibility(View.GONE);
            thisMonthTotalTv.setVisibility(View.GONE);
            thisMonthTotalLucia.setVisibility(View.GONE);
            thisMontTotalRobbert.setVisibility(View.GONE);
        }
        if (isChecked && !isForOtherViewChecked) {
            averageMonthTv.setVisibility(View.GONE);
            averageMonthLucia.setVisibility(View.GONE);
            averageMonthRobbert.setVisibility(View.GONE);
            thisMonthTv.setVisibility(View.GONE);
            thisMonthLucia.setVisibility(View.GONE);
            thisMonthRobbert.setVisibility(View.GONE);
        }
        if (!isChecked && !isOverallTotalsViewChecked) {
            averageMonthTotalTv.setVisibility(View.VISIBLE);
            averageMonthTotalLucia.setVisibility(View.VISIBLE);
            averageMontTotalRobbert.setVisibility(View.VISIBLE);
            thisMonthTotalTv.setVisibility(View.VISIBLE);
            thisMonthTotalLucia.setVisibility(View.VISIBLE);
            thisMontTotalRobbert.setVisibility(View.VISIBLE);
        }
        if (!isChecked && !isForOtherViewChecked) {
            averageMonthTv.setVisibility(View.VISIBLE);
            averageMonthLucia.setVisibility(View.VISIBLE);
            averageMonthRobbert.setVisibility(View.VISIBLE);
            thisMonthTv.setVisibility(View.VISIBLE);
            thisMonthLucia.setVisibility(View.VISIBLE);
            thisMonthRobbert.setVisibility(View.VISIBLE);
        }
        return isChecked;
    }

    private boolean setWeekTotalVisibility(boolean isChecked) {
        if (isChecked && !isOverallTotalsViewChecked) {
            averageWeekTotalTv.setVisibility(View.GONE);
            averageWeekTotalLucia.setVisibility(View.GONE);
            averageWeekTotalRobbert.setVisibility(View.GONE);
            thisWeekTotalTv.setVisibility(View.GONE);
            thisWeekTotalLucia.setVisibility(View.GONE);
            thisWeekTotalRobbert.setVisibility(View.GONE);
        }
        if (isChecked && !isForOtherViewChecked) {
            averageWeekTv.setVisibility(View.GONE);
            averageWeekLucia.setVisibility(View.GONE);
            averageWeekRobbert.setVisibility(View.GONE);
            thisWeekTv.setVisibility(View.GONE);
            thisWeekLucia.setVisibility(View.GONE);
            thisWeekRobbert.setVisibility(View.GONE);
        }
        if (!isChecked && !isOverallTotalsViewChecked) {
            averageWeekTotalTv.setVisibility(View.VISIBLE);
            averageWeekTotalLucia.setVisibility(View.VISIBLE);
            averageWeekTotalRobbert.setVisibility(View.VISIBLE);
            thisWeekTotalTv.setVisibility(View.VISIBLE);
            thisWeekTotalLucia.setVisibility(View.VISIBLE);
            thisWeekTotalRobbert.setVisibility(View.VISIBLE);
        }
        if (!isChecked && !isForOtherViewChecked) {
            averageWeekTv.setVisibility(View.VISIBLE);
            averageWeekLucia.setVisibility(View.VISIBLE);
            averageWeekRobbert.setVisibility(View.VISIBLE);
            thisWeekTv.setVisibility(View.VISIBLE);
            thisWeekLucia.setVisibility(View.VISIBLE);
            thisWeekRobbert.setVisibility(View.VISIBLE);
        }

        return isChecked;
    }

    private void setData() {
        totalInputLucia.setText(String.format("%.2f", lucInputTotal));
        totalInputRobbert.setText(String.format("%.2f", robInputTotal));

        averageInputLucia.setText(String.format("%.2f", (lucTotal / lucCount)));
        averageInputRobbert.setText(String.format("%.2f", (robTotal / robCount)));
        averageInputTotalLucia.setText(String.format("%.2f", (lucInputTotal / lucCount)));
        averageInputTotalRobbert.setText(String.format("%.2f", (robInputTotal / robCount)));

        thisWeekLucia.setText(String.format("%.2f", lucWeekTotalForOther));
        thisWeekTotalLucia.setText(String.format("%.2f", lucWeekTotal));
        thisWeekRobbert.setText(String.format("%.2f", robWeekTotalForOther));
        thisWeekTotalRobbert.setText(String.format("%.2f", robWeekTotal));

        thisMonthLucia.setText(String.format("%.2f", lucMonthTotalForOther));
        thisMonthTotalLucia.setText(String.format("%.2f", lucMonthTotal));
        thisMonthRobbert.setText(String.format("%.2f", robMonthTotalForOther));
        thisMontTotalRobbert.setText(String.format("%.2f", robMonthTotal));

        float lucCurrentStanding = lucTotal - robTotal;
        float robCurrentStanding = robTotal - lucTotal;
        youOweLucia.setText(String.format("%.2f", lucCurrentStanding));
        youOweRobbert.setText(String.format("%.2f", robCurrentStanding));

        float lucTotalStanding = lucInputTotal - robInputTotal;
        float robTotalStanding = robInputTotal - lucInputTotal;
        currentStandingLucia.setText(String.format("%.2f", lucTotalStanding));
        currentStandingRobbert.setText(String.format("%.2f", robTotalStanding));

        String percentageForOtherLucia = String.format("%.2f", lucTotal * 100 / lucInputTotal) + "%";
        String percentageForOtherRobbert = String.format("%.2f", robTotal * 100 / robInputTotal) + "%";
        percentageForOtherLuciaTv.setText(percentageForOtherLucia);
        percentageForOtherRobbertTv.setText(percentageForOtherRobbert);
    }

    private void setBoldText() {
        if (user) {
            totalInputLucia.setTypeface(null, Typeface.BOLD);
            averageInputLucia.setTypeface(null, Typeface.BOLD);
            averageInputTotalLucia.setTypeface(null, Typeface.BOLD);
            thisWeekLucia.setTypeface(null, Typeface.BOLD);
            thisWeekTotalLucia.setTypeface(null, Typeface.BOLD);
            averageWeekLucia.setTypeface(null, Typeface.BOLD);
            averageWeekTotalLucia.setTypeface(null, Typeface.BOLD);
            thisMonthLucia.setTypeface(null, Typeface.BOLD);
            thisMonthTotalLucia.setTypeface(null, Typeface.BOLD);
            averageMonthLucia.setTypeface(null, Typeface.BOLD);
            averageMonthTotalLucia.setTypeface(null, Typeface.BOLD);
            currentStandingLucia.setTypeface(null, Typeface.BOLD);
            percentageForOtherLuciaTv.setTypeface(null, Typeface.BOLD);
            youOweLucia.setTypeface(null, Typeface.BOLD);
        } else {
            totalInputRobbert.setTypeface(null, Typeface.BOLD);
            averageInputRobbert.setTypeface(null, Typeface.BOLD);
            averageInputTotalRobbert.setTypeface(null, Typeface.BOLD);
            thisWeekRobbert.setTypeface(null, Typeface.BOLD);
            thisWeekTotalRobbert.setTypeface(null, Typeface.BOLD);
            averageWeekTotalRobbert.setTypeface(null, Typeface.BOLD);
            averageWeekRobbert.setTypeface(null, Typeface.BOLD);
            thisMonthRobbert.setTypeface(null, Typeface.BOLD);
            thisMontTotalRobbert.setTypeface(null, Typeface.BOLD);
            averageMontTotalRobbert.setTypeface(null, Typeface.BOLD);
            averageMonthRobbert.setTypeface(null, Typeface.BOLD);
            currentStandingRobbert.setTypeface(null, Typeface.BOLD);
            percentageForOtherRobbertTv.setTypeface(null, Typeface.BOLD);
            youOweRobbert.setTypeface(null, Typeface.BOLD);
        }
    }

    private void getAveragesLucia() {
        double monthTotalForOther = 0;
        double monthTotal = 0;
        int monthCount = 0;

        for (int i = 0; i < 12; i++) {
            if (monthTotalLuciaForOther[i] != 0) {
                monthTotalForOther += monthTotalLuciaForOther[i];
                monthTotal += monthTotalLucia[i];
                monthCount++;
            }
        }

        double weekTotalForOther = 0;
        double weekTotal = 0;
        int weekCount = 0;

        for (int i = 0; i < 52; i++) {
            if (weekTotalLuciaForOther[i] != 0) {
                weekTotalForOther += weekTotalLuciaForOther[i];
                weekTotal += weekTotalLucia[i];
                weekCount++;
            }
        }

        averageMonthLucia.setText(String.format("%.2f", monthTotalForOther / monthCount));
        averageMonthTotalLucia.setText(String.format("%.2f", monthTotal / monthCount));
        averageWeekLucia.setText(String.format("%.2f", weekTotalForOther / weekCount));
        averageWeekTotalLucia.setText(String.format("%.2f", weekTotal / weekCount));

        Log.d(LOG_TAG, "Month average Lucia: " + monthTotalForOther / monthCount + " Week average Lucia: " + weekTotalForOther / weekCount);
    }

    private void getAveragesRobbert() {
        double monthTotalForOther = 0;
        double monthTotal = 0;
        int monthCount = 0;

        for (int i = 0; i < 12; i++) {
            if (monthTotalRobbertForOther[i] != 0) {
                monthTotalForOther += monthTotalRobbertForOther[i];
                monthTotal += monthTotalRobbert[i];
                monthCount++;
            }
        }

        double weekTotalForOther = 0;
        double weekTotal = 0;
        int weekCount = 0;

        for (int i = 0; i < 52; i++) {
            if (weekTotalRobbertForOther[i] != 0) {
                weekTotalForOther += weekTotalRobbertForOther[i];
                weekTotal += weekTotalRobbert[i];
                weekCount++;
            }
        }

        averageMonthRobbert.setText(String.format("%.2f", monthTotalForOther / monthCount));
        averageMontTotalRobbert.setText(String.format("%.2f", monthTotal / monthCount));
        averageWeekRobbert.setText(String.format("%.2f", weekTotalForOther / weekCount));
        averageWeekTotalRobbert.setText(String.format("%.2f", weekTotal / weekCount));

        Log.d(LOG_TAG, "Month average Robbert: " + monthTotalForOther / monthCount + " Week average Robbert: " + weekTotalForOther / weekCount);
    }

    private void resetNumbers() {
        for (int i = 0; i < 12; i++) {
            monthTotalRobbertForOther[i] = 0;
            monthTotalRobbert[i] = 0;
            monthTotalLuciaForOther[i] = 0;
            monthTotalLucia[i] = 0;
        }

        for (int i = 0; i < 52; i++) {
            weekTotalRobbertForOther[i] = 0;
            weekTotalRobbert[i] = 0;
            weekTotalLuciaForOther[i] = 0;
            weekTotalLucia[i] = 0;
        }

        todaysWeek = 0;
        todaysMonth = 0;
        todaysYear = 0;

        robWeekTotalForOther = 0;
        lucWeekTotalForOther = 0;

        robWeekTotal = 0;
        lucWeekTotal = 0;

        robMonthTotalForOther = 0;
        lucMonthTotalForOther = 0;

        robMonthTotal = 0;
        lucMonthTotal = 0;

        lucTotal = 0;
        robTotal = 0;

        lucInputTotal = 0;
        robInputTotal = 0;

        lucCount = 0;
        robCount = 0;
    }

    private Bundle saveState(){
        Bundle bundle = new Bundle();
        bundle.putBoolean(OVERALLTOTALS, isOverallTotalsViewChecked);
        bundle.putBoolean(OVERALLFOROTHER, isForOtherViewChecked);
        bundle.putBoolean(TOTALS, isTotalsChecked);
        bundle.putBoolean(MONTHS, isMontsChecked);
        Log.d(LOG_TAG, "Saved state called");
        return bundle;
    }

    @Override
    public void onPause() {
        resetNumbers();
        savedState = saveState();
        super.onPause();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.d(LOG_TAG, "Instance saved");
        outState.putBundle("SavedState", (savedState != null) ? savedState : saveState());
    }

    // TODO set save visibility settings and recover them.

}
