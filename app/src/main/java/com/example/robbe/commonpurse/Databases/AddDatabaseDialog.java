package com.example.robbe.commonpurse.Databases;

import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.Fragments.FragmentContainer;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.Users.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class AddDatabaseDialog extends DialogFragment {
    /**
     * This activity only asks for the password of the Database. The actual database is created in
     * AddDatabaseActivity.java
     */

    public AddDatabaseDialog(){}

    FirebaseAuth firebaseAuth;
    String newDatabaseName, newDatabaseId;

    User currentUser;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String password = null;

        Builder builder = new Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_database_dialog, null);


        firebaseAuth = FirebaseAuth.getInstance();
        TextView addDatabaseTitle = dialogView.findViewById(R.id.add_database_title);
        EditText addDatabaseEditText = dialogView.findViewById(R.id.add_database_name_edit_text);
        EditText passwordEditText = dialogView.findViewById(R.id.add_database_password_edit_text);

        currentUser = getUserData();

        Bundle bundle = getArguments();
        if (bundle != null){
            addDatabaseEditText.setVisibility(View.GONE);
            addDatabaseTitle.setText(R.string.enter_your_password);
            password = bundle.getString(Constants.PASSWORD);
            newDatabaseName = bundle.getString(Constants.DATABASE_NAME);
            newDatabaseId = bundle.getString(Constants.DATABASE_ID);
        }

        String finalPassword = password;
        builder.setView(dialogView)
                .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("Save", (dialog, which) -> {
                    if (bundle == null) {
                        Toast.makeText(getActivity(), "Something has gone wrong here...", Toast.LENGTH_SHORT).show();
                    } else {
                        if (passwordEditText.getText().toString().equals(finalPassword)){
                            Intent intent = new Intent(getActivity(), FragmentContainer.class);
                            intent.putExtra("DatabaseName", newDatabaseName);

                            SharedPreferences preferences = getActivity().getSharedPreferences(Constants.SAVED_DATABASE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(Constants.SAVED_DATABASE, newDatabaseName);
                            editor.putString(Constants.SAVED_DATABASE_ID, newDatabaseId);
                            editor.apply();

                            startActivity(intent);
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), "Password incorrect", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        return builder.create();
    }

    private User getUserData() {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        String name = currentUser.getDisplayName();
        String id = currentUser.getUid();
        String email = currentUser.getEmail();
        String role = Constants.USER_ROLE_NORMAL;
        long timeCreated = System.currentTimeMillis();

        return new User(name, id, email, role, timeCreated);
    }
}
