package com.example.robbe.commonpurse.Users;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.R;

import java.util.regex.Pattern;

public class AddUserActivity extends AppCompatActivity {

    private EditText firstNameEt, lastNameEt, emailEt;
    private ImageView userPictureView;
    private Uri selectedPictureUri = null;
    private User newUser = new User();
    private int requestCodePhoto = 2;

    private String role;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == requestCodePhoto && resultCode == RESULT_OK){
            selectedPictureUri = data.getData();
            createLog("The photo URL is: " + selectedPictureUri);

            Glide.with(this)
                    .load(selectedPictureUri)
                    .into(userPictureView);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        firstNameEt = findViewById(R.id.add_user_name_et);
        lastNameEt = findViewById(R.id.add_user_last_name_et);
        emailEt = findViewById(R.id.add_user_email_et);
        Spinner roleSpinner = findViewById(R.id.add_user_role_spinner);
        userPictureView = findViewById(R.id.add_user_picture_view);
        Button saveButton = findViewById(R.id.add_user_save_button);

        Intent intent = getIntent();

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.roles));
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roleSpinner.setAdapter(spinnerAdapter);

        roleSpinner.setSelection(spinnerAdapter.getPosition(getResources().getString(R.string.administrator)));

        try {
            newUser = (User) intent.getSerializableExtra(Constants.SEND_USER);
            createLog("New user id: " + newUser.getId());

            if (newUser.getFirstName() != null){
                firstNameEt.setText(newUser.getFirstName());
            } else {
                String[] name = newUser.getName().split(" ");
                firstNameEt.setText(name[0]);


                if (name[1] != null) {
                    String lastName = "";
                    for (int i = 1; i < name.length; i++) {
                        lastName = lastName + name[i];
                    }
                    lastNameEt.setText(lastName);
                }
            }

            if (newUser.getLastName() != null){
                lastNameEt.setText(newUser.getLastName());
            }

            if (newUser.getEmailAddress() != null){
                emailEt.setText(newUser.getEmailAddress());
            }

            if (newUser.getRole() != null){
                roleSpinner.setSelection(spinnerAdapter.getPosition(newUser.getRole()));
            }

            if (newUser.getPhotoId() != null){
                Glide.with(this).load(Uri.parse(newUser.getPhotoId())).into(userPictureView);
            }

        } catch (Exception e){
            createLog("Intent has no Serializable extra: " + e.getMessage());
        }

        roleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                role = String.valueOf(parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        userPictureView.setOnClickListener(v -> {
            Intent getExistingPhotoIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getExistingPhotoIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            getExistingPhotoIntent.setType("image/jpeg");
            startActivityForResult(getExistingPhotoIntent, requestCodePhoto);
        });

        saveButton.setOnClickListener(v -> {
            if (newUser.getName() == null && newUser.getId() == null){
                newUser = new User();
            }

            createLog("New user id 2: " + newUser.getId());

            String firstName = firstNameEt.getText().toString().trim();
            String lastName = lastNameEt.getText().toString().trim();
            String fullName = firstName + " " + lastName;
            String emailAddress = emailEt.getText().toString().trim();
            long time = System.currentTimeMillis();

            if (firstName.length() < 2){
                Toast.makeText(this, "Your first name is too short!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (lastName.length() < 2){
                Toast.makeText(this, "Your last name is too short!", Toast.LENGTH_LONG).show();
            }

            if (!checkEmailAddress(emailAddress)){
                if(emailAddress.length() > 0){
                    Toast.makeText(this, "This is not a valid email address", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Toast.makeText(this, "You have not entered a valid email address. No email address will be in the system", Toast.LENGTH_SHORT).show();
                    emailAddress = "No email address";
                }
            }

            newUser.setFirstName(firstName);
            newUser.setLastName(lastName);
            newUser.setName(fullName);
            newUser.setEmailAddress(emailAddress);
            newUser.setRole(role);

            if (selectedPictureUri != null) {
                newUser.setPhotoUri(String.valueOf(selectedPictureUri));
            }

            if (newUser.getTimeCreated() == 0 || newUser.getId() == null) {
                newUser.setTimeCreated(time);
                newUser.setId(time + fullName);
            }

            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.RETURN_USER, newUser);
            setResult(RESULT_OK, returnIntent);
            finish();
        });
    }

    private boolean checkEmailAddress(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void createLog(String message){
        Log.d("AddUserActivity", message);
    }

}
