package com.example.robbe.commonpurse.Databases;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.robbe.commonpurse.CheckConnectivity;
import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.Fragments.FragmentContainer;
import com.example.robbe.commonpurse.R;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChooseDatabaseToJoin extends AppCompatActivity{

    private static final String LOG_TAG = "ChooseDatabaseToJoin";
    private static final String NO_DATABASE_FOUND = "No databases found";

    FirebaseDatabase database;
    DatabaseReference databasesReference;
    ChildEventListener childEventListener;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        MenuItem addNote = menu.findItem(R.id.action_add_note);
        addNote.setVisible(false);
        MenuItem settingsMenu = menu.findItem(R.id.settings_menu);
        settingsMenu.setVisible(false);
        MenuItem addUserMenu = menu.findItem(R.id.add_new_user);
        addUserMenu.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_go_to_addPayment:
                if (CheckConnectivity.hasInternetConnection(this)) {
                    Intent addDatabaseIntent = new Intent(this, AddDatabaseActivity.class);
                    startActivity(addDatabaseIntent);
                } else {
                    Toast.makeText(this, "You need a connection to the " +
                            "internet in order to create a new database", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.log_out_menu:
                AuthUI.getInstance()
                        .signOut(ChooseDatabaseToJoin.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_database_to_join);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        database = FirebaseDatabase.getInstance();
        databasesReference = database.getReference().child(Constants.DATABASES);

        Log.d(LOG_TAG, "ChooseDatabase started");
        ListView listView = findViewById(R.id.choose_database_list_view);
        ArrayList<Databases> databaseList = new ArrayList<>();
        DatabasesAdapter adapter = new DatabasesAdapter(this, android.R.layout.simple_list_item_1, databaseList);
        listView.setAdapter(adapter);

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Databases db = dataSnapshot.getValue(Databases.class);
                db.setKey(dataSnapshot.getKey());
                adapter.insert(db, 0);
                Log.d(LOG_TAG, "A database has been found.");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        databasesReference.addChildEventListener(childEventListener);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            Databases database = (Databases) listView.getItemAtPosition(position);

            if (database.getName().equals(NO_DATABASE_FOUND)){
                Toast.makeText(this, "You have selected no database", Toast.LENGTH_SHORT).show();
                return;
            }

            if (database.getPassword() == null){
                Intent intent = new Intent(ChooseDatabaseToJoin.this, FragmentContainer.class);
                intent.putExtra("DatabaseName", database.getName());

                SharedPreferences preferences = getSharedPreferences(Constants.SAVED_DATABASE, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.SAVED_DATABASE, database.getName());
                editor.putString(Constants.SAVED_DATABASE_ID, String.valueOf(database.getTimeCreated()));
                editor.apply();

                Log.d(LOG_TAG, "Database chosen: " + database.getName());

                startActivity(intent);
            } else if (database.getPassword() != null){
                AddDatabaseDialog dialog = new AddDatabaseDialog();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PASSWORD, database.getPassword());
                bundle.putString(Constants.DATABASE_NAME, database.getName());
                bundle.putString(Constants.DATABASE_ID, String.valueOf(database.getTimeCreated()));
                dialog.setArguments(bundle);
                dialog.show(getSupportFragmentManager(), "CheckPasswordDialog");
            } else {
                Toast.makeText(this, "Something has gone wrong. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
