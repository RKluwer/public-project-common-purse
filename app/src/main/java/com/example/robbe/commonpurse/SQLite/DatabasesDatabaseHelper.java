package com.example.robbe.commonpurse.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.robbe.commonpurse.Constants.DatabaseEntry;

public class DatabasesDatabaseHelper extends SQLiteOpenHelper {

    String tableName = null;

    public DatabasesDatabaseHelper(Context context, String tableName){
        super(context, tableName, null, 1);
        this.tableName = tableName;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + tableName + " ("
                + DatabaseEntry.COL0_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseEntry.COL1_FIREBASEID + " TEXT NOT NULL, "
                + DatabaseEntry.COL2_TIMECREATED + " INTEGER, "
                + DatabaseEntry.COL3_DATABASENAME + " TEXT NOT NULL, "
                + DatabaseEntry.COL4_LASTACCESSED + " INTEGER, "
                + DatabaseEntry.COL5_PASSWORD + " TEXT NOT NULL, "
                + DatabaseEntry.COL6_CURRENCY + " TEXT)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF TABLE EXISTS " + tableName);
        onCreate(db);
    }
}
