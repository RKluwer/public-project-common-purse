package com.example.robbe.commonpurse.Fragments;

import android.app.AlertDialog;
import android.database.Cursor;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.robbe.commonpurse.Constants;

import com.example.robbe.commonpurse.Payments.AddPaymentDialog;
import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.Payments.PaymentCursorAdapter;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.SQLite.CursorToArrayConverter;
import com.example.robbe.commonpurse.Users.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.database.FirebaseDatabase;
import com.example.robbe.commonpurse.Constants.PaymentEntry;
import com.example.robbe.commonpurse.Constants.UserEntry;

import java.util.ArrayList;

/**
 * Created by robbe on 18-11-2017.
 */

public class PaymentOverviewRight extends Fragment implements LoaderCallbacks<Cursor> {

    private String databaseName, leftSpinnerValue, rightSpinnerValue;
    private static final int LOAD_ALL_PAYMENTS = 101;
    protected static final int LOAD_VISIBLE_PAYMENTS = 100;
    private static final int LOAD_USER_PAYMENTS_RECEIVED = 102;
    private static final int LOAD_USER_PAYMENTS_MADE = 103;
    private static final int LOAD_FILTERED_PAYMENTS = 104;

    private static final int LOAD_ALL_USERS = 200;

    View rootView;
    boolean spinnerVisibility = true;

    private PaymentCursorAdapter mPaymentCursorAdapter;
    private ProgressBar mProgressBar;
    private LinearLayout spinnerLayout;
    private Spinner leftSpinner, rightSpinner;
    private ImageButton searchButton;
    private EditText searchEditText;

    private ArrayList<Payment> payments, allPayments;
    private ArrayList<User> userList;

    public PaymentOverviewRight() {
        // Required public constructor.
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            leftSpinnerValue = savedInstanceState.getString(Constants.LEFT_SPINNER);
            rightSpinnerValue = savedInstanceState.getString(Constants.RIGHT_SPINNER);
        }

        rootView = inflater.inflate(R.layout.payment_overview, container, false);

        FragmentContainer fc = (FragmentContainer) getActivity();
        databaseName = fc.getDatabaseName();

        ListView mListView = rootView.findViewById(R.id.list);
        mProgressBar = rootView.findViewById(R.id.progressBar);
        spinnerLayout = rootView.findViewById(R.id.payment_overview_spinner_layout);
        leftSpinner = rootView.findViewById(R.id.payment_overview_spinner1);
        rightSpinner = rootView.findViewById(R.id.payment_overview_spinner2);
        searchButton = rootView.findViewById(R.id.payment_overview_search);
        searchEditText = rootView.findViewById(R.id.payment_overview_search_et);
        userList = new ArrayList<>();

        payments = new ArrayList<>();
        mPaymentCursorAdapter = new PaymentCursorAdapter(getActivity(), null);
        mListView.setAdapter(mPaymentCursorAdapter);

        View emptyView = rootView.findViewById(R.id.no_entries_text_view);
        mListView.setEmptyView(emptyView);

        // Initiate loading the users into the spinners
        // This action, upon completion is followed by the loading of the payments
        //startLoadingAllPayments();

        // This is the loop button in the UI. Once clicked the search edit text appears, and the
        // spinners disappear. Once clicked again and the opposite will happen.
        searchButton.setOnClickListener(v -> {
            if (searchEditText.getVisibility() == View.GONE){
                leftSpinner.setVisibility(View.GONE);
                rightSpinner.setVisibility(View.GONE);
                searchEditText.setVisibility(View.VISIBLE);
                searchEditText.requestFocus();
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            } else {
                searchEditText.setVisibility(View.GONE);
                rightSpinner.setVisibility(View.VISIBLE);
                leftSpinner.setVisibility(View.VISIBLE);
            }
        });

        // This is an edit Text Change Listener. Once the user puts in text in the search Edit Text,
        // the result query is filtered by restarting the support loader  manager.
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constants.DESCRIPTION, new String[]{"%" + s.toString() + "%",
                        String.valueOf(Constants.TRUE)});

                getActivity().getSupportLoaderManager().restartLoader(LOAD_FILTERED_PAYMENTS,
                        bundle, PaymentOverviewRight.this);
            }
        });

        mListView.setOnItemLongClickListener((adapterView, view, position, l) -> {
            Cursor c = (Cursor) mPaymentCursorAdapter.getItem(position);
            final Payment currentPayment = CursorToArrayConverter.getFirstPayment(c);
            final Long currentPaymentTime = currentPayment.getmTime();
            ArrayList<String> keyList = new ArrayList<>();

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setMessage("Do you want to delete this entry?");
            builder.setPositiveButton("Delete", (dialogInterface, i1) -> {
                //mPaymentDatabaseReference.child(currentPayment.getKey()).removeValue();
            });
            builder.setNeutralButton("Edit", (dialogInterface, i12) -> {
                for (Payment p : allPayments) {
                    if (currentPaymentTime.equals(p.getmTime())) {
                        keyList.add(p.getKey());
                    }
                }
                AddPaymentDialog adp = new AddPaymentDialog();
                adp.setKeys(keyList);
                adp.setDatabase(databaseName);
                adp.show(getActivity().getSupportFragmentManager(), "Edit or Delete Dialog");
            });

            builder.show();

            return false;
        });

        mListView.setOnItemClickListener((adapterView, view, i, l) -> {
            TextView tv = view.findViewById(R.id.payment_details_tv);
            if (tv.getVisibility() == View.VISIBLE) {
                tv.setVisibility(View.GONE);
            } else {
                tv.setVisibility(View.VISIBLE);
            }
        });

        leftSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                leftSpinnerValue = parent.getItemAtPosition(position).toString().trim();
                createLog("Left spinner value is: " + leftSpinnerValue);

                loadPayments();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                createLog("Nothing selected in the left spinner");
            }
        });

        rightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rightSpinnerValue = parent.getItemAtPosition(position).toString().trim();
                createLog("Right spinner value is: " + rightSpinnerValue);

                loadPayments();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                createLog("Nothing selected in the right spinner");
            }
        });

        return rootView;
    }

    public void startLoadingAllPayments() {
        mPaymentCursorAdapter.swapCursor(null);

       getActivity().getSupportLoaderManager().restartLoader(LOAD_ALL_PAYMENTS, null, PaymentOverviewRight.this);
    }

    private void loadUsers() {
        getActivity().getSupportLoaderManager().restartLoader(LOAD_ALL_USERS, null, PaymentOverviewRight.this);
    }

    public void setSpinnerLayout(boolean visible) {
        spinnerVisibility = visible;
    }

    private void setupSpinners(ArrayList<String> usersArray) {
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(getActivity(),
                R.array.for_or_from, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        leftSpinner.setAdapter(adapter1);

        if (leftSpinnerValue != null) {
            int position = adapter1.getPosition(leftSpinnerValue);
            leftSpinner.setSelection(position);
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, usersArray);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rightSpinner.setAdapter(adapter2);

        if (rightSpinnerValue != null) {
            int position = adapter2.getPosition(rightSpinnerValue);
            rightSpinner.setSelection(position);
        }
    }

    public void loadPayments() {
        createLog("Spinner visibility is: " + spinnerVisibility);
        String[] paymentDirection = getActivity()
                .getResources()
                .getStringArray(R.array.for_or_from);

        boolean paymentMade = true;
        Bundle bundle = new Bundle();


        if (leftSpinnerValue != null) {
            paymentMade = leftSpinnerValue.equals(paymentDirection[0]);
        }

        if (rightSpinnerValue == null){
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
            rightSpinnerValue = currentUser.getDisplayName();
        }

        if (paymentMade) {
            bundle.putStringArray(Constants.USER_ARG, new String[]{rightSpinnerValue, String.valueOf(Constants.TRUE)});
            getActivity().getSupportLoaderManager().restartLoader(LOAD_USER_PAYMENTS_MADE, bundle, PaymentOverviewRight.this);
        } else {
            bundle.putStringArray(Constants.USER_ARG, new String[]{rightSpinnerValue});
            getActivity().getSupportLoaderManager().restartLoader(LOAD_USER_PAYMENTS_RECEIVED, bundle, PaymentOverviewRight.this);
        }
    /*
        // This is the eventListener that updates the payments in the ListView
        // It also updates the total amount in the bottom of the screen.
        ChildEventListener mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final Payment payment = dataSnapshot.getValue(Payment.class);
                payment.setKey(dataSnapshot.getKey());
                allPayments.add(payment);


                if (payment.isBasePayment()) {
                    //mPaymentCursorAdapter.insert(payment, 0);
                }

                if (noEntryTv.getVisibility() == View.VISIBLE) {
                    noEntryTv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                String key = dataSnapshot.getKey();
                Payment updatedPayment = dataSnapshot.getValue(Payment.class);
                for (Payment pm : payments) {
                    if (pm.getKey().equals(key) && pm.getKey() != null) {
                        pm.updateValues(updatedPayment);
                        Toast.makeText(getActivity(), "Amount removed = " + pm.getAmount()
                                + " + " + updatedPayment.getAmount(), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                for (Payment pm: allPayments){
                    if (pm.getKey().equals(key) && pm.getKey() != null) {
                        pm.updateValues(updatedPayment);
                        return;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                for (Payment pm : payments) {
                    if (pm.getKey().equals(key) && pm.getKey() != null) {
                        //mPaymentCursorAdapter.remove(pm);
                        allPayments.remove(pm);
                        return;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        mPaymentDatabaseReference.addChildEventListener(mChildEventListener);

        mPaymentDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProgressBar.setVisibility(View.GONE);
                noEntryTv.setVisibility(View.GONE);

                mPaymentCursorAdapter.setAllPayments(allPayments);

                if (mPaymentCursorAdapter.getCount() == 0){
                    noEntryTv.setVisibility(View.VISIBLE);

                    Toast.makeText(getActivity(), "No Payments have been made yet", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        */
    }

    private void setPaymentData(Cursor data) {
        mPaymentCursorAdapter.setAllPayments(CursorToArrayConverter.getPaymentArrayList(data));
    }

    private void setUserData(Cursor data) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        userList = CursorToArrayConverter.getUserArrayList(data);

        for (User u: userList){
            if (u.getName().equals(currentUser)){
                rightSpinnerValue = u.getName();
            }
        }

        // Creating an array with only name Strings so that the array adapter can put them in
        // the dropdown menu.
        ArrayList<String> usersArray = new ArrayList<>();
        for (User u: userList) {
            usersArray.add(u.getName());
        }

        if (usersArray.size() < 2 || !spinnerVisibility) {
            spinnerLayout.setVisibility(View.GONE);
        } else {
            setupSpinners(usersArray);
        }
    }

    private void createLog(String message) {
        Log.d("PaymentOverviewRight", message);
    }

    @Override
    public void onDestroy() {
        try {
            mPaymentCursorAdapter.changeCursor(null);
        } catch (NullPointerException e) {
            createLog(e.getMessage());
        }
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Constants.LEFT_SPINNER, leftSpinnerValue);
        outState.putString(Constants.RIGHT_SPINNER, rightSpinnerValue);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String whereMainLineTrue = PaymentEntry.COL14_BASEPAYMENT + " = ?";
        String selectionArgTrue[] = new String[]{String.valueOf(Constants.TRUE)};

        switch (id) {
            case LOAD_ALL_PAYMENTS:
                return new CursorLoader(
                        getActivity(),
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        null,
                        null,
                        null
                );
            case LOAD_VISIBLE_PAYMENTS:
                return new CursorLoader(
                        getActivity(),
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        whereMainLineTrue,
                        selectionArgTrue,
                        PaymentEntry.COL11_TIME + " DESC"
                );
            case LOAD_USER_PAYMENTS_MADE:
                return new CursorLoader(
                        getActivity(),
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        PaymentEntry.COL3_USER + " = ? AND "
                                + PaymentEntry.COL14_BASEPAYMENT + " = ?",
                        args.getStringArray(Constants.USER_ARG),
                        PaymentEntry.COL11_TIME + " DESC"
                );
            case LOAD_USER_PAYMENTS_RECEIVED:
                return new CursorLoader(
                        getActivity(),
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        PaymentEntry.COL6_RECEIVER + " = ?",
                        args.getStringArray(Constants.USER_ARG),
                        PaymentEntry.COL11_TIME + " DESC"
                );
            case LOAD_FILTERED_PAYMENTS:
                return new CursorLoader(
                        getActivity(),
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        PaymentEntry.COL10_DESCRIPTION + " LIKE ? AND "
                                + PaymentEntry.COL14_BASEPAYMENT + " = ?",
                        args.getStringArray(Constants.DESCRIPTION),
                        PaymentEntry.COL11_TIME + " DESC"
                );
            case LOAD_ALL_USERS:
                return new CursorLoader(
                        getActivity(),
                        UserEntry.CONTENT_URI_USERS,
                        UserEntry.PROJECT_ALL,
                        null,
                        null,
                        null
                );
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int loaderID = loader.getId();
        switch (loaderID){
            case LOAD_VISIBLE_PAYMENTS:
            case LOAD_FILTERED_PAYMENTS:
            case LOAD_USER_PAYMENTS_MADE:
            case LOAD_USER_PAYMENTS_RECEIVED:
                mPaymentCursorAdapter.swapCursor(data);
                mProgressBar.setVisibility(View.GONE);
                createLog("There are " + data.getCount() + " payments loaded from the database");
                break;
            case LOAD_ALL_USERS:
                createLog("There are " + data.getCount() + " users loaded from the database");
                setUserData(data);
                loadPayments();
                break;
            case LOAD_ALL_PAYMENTS:
                setPaymentData(data);
                loadUsers();
                break;
            default:
                createLog("Loader Id not recognized");
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mPaymentCursorAdapter.swapCursor(null);
    }
}
