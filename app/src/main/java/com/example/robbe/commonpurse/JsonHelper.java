package com.example.robbe.commonpurse;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by robbe on 24-11-2017.
 */

public class JsonHelper {

    JsonHelper(){}

    public static URL createUrl(String earthquakeUrl) {
        URL urls = null;
        try {
            urls = new URL(earthquakeUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        return urls;
    }

    public static String makeHTTPRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            return jsonResponse;
        }
        HttpURLConnection connection = null;
        InputStream inputStream = null;

        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(1000 /* milliseconds */);
            connection.setConnectTimeout(1500 /* milliseconds */);
            connection.connect();

            if (connection.getResponseCode() == 200) {
                inputStream = connection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    public static String getExchangeRate(String json, String otherCurrency, String mainCurrency) {

        String exchangeRate = null;

        JSONObject jsonObject = null;
        JSONObject jsonObject1 = null;

        try {
            jsonObject = new JSONObject(json);
            jsonObject1 = jsonObject.getJSONObject(otherCurrency + "_" + mainCurrency);
            Log.d("JsonHelper", jsonObject1.toString());

            exchangeRate = jsonObject1.getString("val");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return exchangeRate;
    }
}
