package com.example.robbe.commonpurse.Payments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.example.robbe.commonpurse.Constants.PaymentEntry;

/**
 * Created by robbe on 18-11-2017.
 */

public class PaymentCursorAdapter extends CursorAdapter {

    public PaymentCursorAdapter(Context context, Cursor cursor) {super(context, cursor, 0);}

    private ArrayList<Payment> allPayments = new ArrayList<>();

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView descriptionView = view.findViewById(R.id.description_text_view);
        TextView timeView = view.findViewById(R.id.time_text_view);
        TextView amountView = view.findViewById(R.id.amount_text_view);
        TextView originalAmountView = view.findViewById(R.id.original_amount_view);
        TextView paymentDetailView = view.findViewById(R.id.payment_details_tv);

        // Setting the description on the Description Text View in the list item.
        String description = cursor.getString(cursor.getColumnIndexOrThrow(PaymentEntry.COL10_DESCRIPTION));
        descriptionView.setText(description);

        // Setting the time and user on the Time View.
        Long time = cursor.getLong(cursor.getColumnIndexOrThrow(PaymentEntry.COL11_TIME));
        String user = cursor.getString(cursor.getColumnIndexOrThrow(PaymentEntry.COL3_USER));
        DateFormat sdf = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
        Date resultDate = new Date(time);
        StringBuilder sBuilder = new StringBuilder();

        sBuilder.append("Payment made on: ");
        sBuilder.append(sdf.format(resultDate));
        sBuilder.append(" by: ");
        String[] userFirstName = user.split(" ");
        sBuilder.append(userFirstName[0]);
        timeView.setText(sBuilder);

        // Setting the amount with 2 decimals on the amount view.
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        float a = cursor.getFloat(cursor.getColumnIndexOrThrow(PaymentEntry.COL8_TOTAL_AMOUNT));
        String amount = decimalFormat.format(a);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String baseCurrency = preferences.getString("currency_pref", "EUR");
        String amountText = baseCurrency + " " + amount;
        amountView.setText(amountText);

        // Setting the original amount (amount in original currency) under the amount text view
        String currency = cursor.getString(cursor.getColumnIndexOrThrow(PaymentEntry.COL2_CURRENCY));
        if (!baseCurrency.equals(currency)) {
            originalAmountView.setVisibility(View.VISIBLE);
            float oa = cursor.getFloat(cursor.getColumnIndexOrThrow(PaymentEntry.COL9_ORIGINAL_AMOUNT));
            String originalAmount = decimalFormat.format(oa);
            String concatText = currency + " " + originalAmount;
            originalAmountView.setText(concatText);
        } else {
            originalAmountView.setVisibility(View.GONE);
        }

        // Setting the receivers of the payment in the list view, when clicked upon.
        // Every receiver has a new line.
        StringBuilder paymentDetailBuilder = new StringBuilder();

        for (Payment p: allPayments){
            if (time.equals(p.getmTime())){
                paymentDetailBuilder.append(System.getProperty("line.separator"));
                paymentDetailBuilder.append(p.toShortString());
            }
        }
        paymentDetailView.setText(paymentDetailBuilder);

        int isOfflineDeleted = cursor.getInt(cursor.getColumnIndexOrThrow(PaymentEntry.COL15_ISPOSTED));
        if (isOfflineDeleted == Constants.TO_BE_DELETED){
            view.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
        } else {
            view.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_left);
        view.startAnimation(animation);
    }

    // Receive the list with all payments for later iteration.
    public void setAllPayments(ArrayList<Payment> allPayments){
        if (allPayments == null){
            allPayments = new ArrayList<>();
        }

        this.allPayments = allPayments;
    }

    public void createLog (String message){
        Log.d("PaymentCursorAdapter", message);
    }
}
