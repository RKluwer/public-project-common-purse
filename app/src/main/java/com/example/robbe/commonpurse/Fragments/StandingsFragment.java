package com.example.robbe.commonpurse.Fragments;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.Payments.Payment;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.SQLite.CursorToArrayConverter;
import com.example.robbe.commonpurse.Users.User;
import com.example.robbe.commonpurse.Constants.PaymentEntry;
import com.example.robbe.commonpurse.Constants.UserEntry;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StandingsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOAD_ALL_USERS = 500;
    private static final int LOAD_ALL_PAYMENTS = 510;

    private ProgressBar progressBar;
    private User currentUser;

    private String databaseName;
    private ArrayList<Payment> paymentArrayList;
    private List<User> standingArrayList = new ArrayList<>();
    private UserStandingAdapter userStandingAdapter;
    private UserStandingPaymentAdapter uspa;
    private ArrayList<User> copiedStandingArrayList = new ArrayList<>();
    private ArrayList<Payment> standingPaymentArrayList;

    private TextView nameTv;
    private GridView gridView;
    private ImageView userPictureView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            try {
                currentUser = (User) savedInstanceState.getSerializable(Constants.STANDING_FRAGMENT_USER);
                setSpecificUserAdapter(currentUser);
            } catch (NullPointerException e) {
                createLog("No current user saved");
                createLog(e.getMessage());
            }

            databaseName = savedInstanceState.getString(Constants.DATABASE_NAME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.standings_fragment, container, false);

        progressBar = rootView.findViewById(R.id.standing_fragment_progressBar);

        ImageButton previousButton = rootView.findViewById(R.id.button_click_left);
        ImageButton nextButton = rootView.findViewById(R.id.button_click_right);
        userPictureView = rootView.findViewById(R.id.user_standing_profile_picture);

        gridView = rootView.findViewById(R.id.standing_fragment_grid_view);
        nameTv = rootView.findViewById(R.id.standing_fragment_name_tv);
        nameTv.setText(getContext().getResources().getString(R.string.all_users));

        nextButton.setOnClickListener(v -> {
            if (currentUser == null) {
                return;
            }

            if (gridView.getAdapter().equals(uspa)) {
                int index = standingArrayList.indexOf(currentUser);

                if (index == standingArrayList.size() - 1) {
                    index = 0;
                } else {
                    index++;
                }

                currentUser = standingArrayList.get(index);
            }

            setSpecificUserAdapter(currentUser);
        });

        previousButton.setOnClickListener(v -> {
            if (gridView.getAdapter().equals(userStandingAdapter)) {
                return;
            }

            currentUser = null;
            nameTv.setText(getContext().getResources().getString(R.string.all_users));
            setOverviewAdapter();
        });

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            try {
                currentUser = (User) parent.getItemAtPosition(position);
            } catch (Exception e) {
                createLog("No current user found");
                return;
            }

            setSpecificUserAdapter(currentUser);
        });

        startLoadingData();

        return rootView;
    }

    public void startLoadingData(){
        clearData();

        getActivity().getSupportLoaderManager().restartLoader(LOAD_ALL_USERS, null, StandingsFragment.this);
    }

    private void loadUsers(Cursor data) {
        standingArrayList = CursorToArrayConverter.getUserArrayList(data);

        getActivity().getSupportLoaderManager().restartLoader(LOAD_ALL_PAYMENTS, null, StandingsFragment.this);
    }

    private void loadPayments(Cursor data) {
        paymentArrayList = CursorToArrayConverter.getPaymentArrayList(data);
        calculateStandings();

        progressBar.setVisibility(View.GONE);
    }

    private void calculateStandings() {
        for (Payment p : paymentArrayList) {
            for (User u : standingArrayList) {
                if (p.getReceiver().equals(u.getName())) {
                    if (p.getReceiver().equals(p.getUser())) {
                        float userAmount = u.getUserAmount();
                        userAmount += p.getTotalAmount();
                        userAmount -= p.getAmount();

                        u.setUserAmount(userAmount);

                    } else {
                        float userAmount = u.getUserAmount();
                        userAmount -= p.getAmount();
                        u.setUserAmount(userAmount);
                    }
                    //createLog(u.getName() + "'s amount is: " + u.getUserAmount());
                }
            }
        }

        // Comparing the amount on the user class.
        Collections.sort(standingArrayList, (o1, o2) -> (int) (o2.getUserAmount() - o1.getUserAmount()));

        for (User u : standingArrayList) {
            User newUser = new User();
            newUser.setUserAmount(u.getUserAmount());
            newUser.setName(u.getName());
            copiedStandingArrayList.add(newUser);
        }

        if (currentUser == null) {
            setOverviewAdapter();
        } else {
            setSpecificUserAdapter(currentUser);
        }

        progressBar.setVisibility(View.GONE);
    }

    private ArrayList<Payment> calculatePayments() {
        ArrayList<Payment> userPayments = new ArrayList<>();

        Collections.sort(copiedStandingArrayList, (o1, o2) -> (int) (o2.getUserAmount() - o1.getUserAmount()));

        do {

            User highestAmountUser = null;
            User lowestAmountUser = null;
            try {
                highestAmountUser = copiedStandingArrayList.get(0);
                lowestAmountUser = copiedStandingArrayList.get(copiedStandingArrayList.size() - 1);

            } catch (IndexOutOfBoundsException e) {
                createLog(e.getMessage());
            }

            // Making sure that the coming calculations can be executed.
            if (lowestAmountUser == null) {
                createLog("An error has occurred, lowest amount user is null");
                return null;
            }

            if (highestAmountUser == null) {
                createLog("An error has occurred, lowest amount user is null");
                return null;
            }

            if (highestAmountUser.getUserAmount() < 0.5) {
                copiedStandingArrayList.remove(0);
            }

            if (lowestAmountUser.getUserAmount() * -1 < 0.5) {
                copiedStandingArrayList.remove(copiedStandingArrayList.size() - 1);
            }

            Payment p1 = new Payment();
            p1.setUser(lowestAmountUser.getName());
            p1.setReceiver(highestAmountUser.getName());

            // Creating a payment when the lowest amount user has less debt than the highest amount
            // user is owed.
            if (lowestAmountUser.getUserAmount() * -1 < highestAmountUser.getUserAmount()) {
                p1.setAmount(lowestAmountUser.getUserAmount() * -1);

                // lowestAmountUser amount is always negative.
                float leftoverAmount = highestAmountUser.getUserAmount();
                leftoverAmount += lowestAmountUser.getUserAmount();
                highestAmountUser.setUserAmount(leftoverAmount);

                try {
                    copiedStandingArrayList.remove((copiedStandingArrayList.size() - 1));
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.getStackTrace();
                    createLog("Copied standing array list size is: " + copiedStandingArrayList.size()
                            + " and the index is: " + (copiedStandingArrayList.size() - 1));
                }
                userPayments.add(p1);
            }

            // Creating a payment for when the highest amount user is owed more than the lowest amount
            // user is indebted.

            else if (lowestAmountUser.getUserAmount() * -1 > highestAmountUser.getUserAmount()) {
                p1.setAmount(highestAmountUser.getUserAmount());

                float leftoverAmount = lowestAmountUser.getUserAmount();
                leftoverAmount += highestAmountUser.getUserAmount();
                lowestAmountUser.setUserAmount(leftoverAmount);

                copiedStandingArrayList.remove(0);
                userPayments.add(p1);
            }

            // Creating a payment for when both amounts are the same.

            else if (lowestAmountUser.getUserAmount() * -1 == highestAmountUser.getUserAmount()) {
                p1.setAmount(highestAmountUser.getUserAmount());

                copiedStandingArrayList.remove(copiedStandingArrayList.size() - 1);
                copiedStandingArrayList.remove(0);

                userPayments.add(p1);
            }
        } while (copiedStandingArrayList.size() > 1);

        createLog("user payments size is: " + userPayments.size());
        return userPayments;
    }

    private void createLog(String message) {
        Log.d("StandingsFragment", message);
    }

    private void setOverviewAdapter() {
        userPictureView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.smile));

        userStandingAdapter = new UserStandingAdapter(getActivity()
                , R.layout.user_staning_list_item
                , standingArrayList);
        gridView.setAdapter(userStandingAdapter);
    }

    private void setSpecificUserAdapter(User user) {
        nameTv.setText(user.getName());

        if (user.getPhotoUri() != null) {
            String tempUri = user.getPhotoUri();
            createLog("the TempURI is: " + tempUri);
            try {
                // If the user has a photo saved offline this will be loaded.
                Glide.with(getActivity())
                        .load(getTempFile(Uri.parse(tempUri)))
                        .into(userPictureView);
                createLog("Picture set from the internal memory");
            } catch (Exception e) {
                createLog(e.getMessage());
                //If the user has a photo online, but not yet off line, the photo is loaded from the
                // internet rather than the internal storage.
                Glide.with(getActivity())
                        .load(user.getPhotoId())
                        .into(userPictureView);
                createLog("Loaded the picture from the internet and set it in the view.");
            }
        } else {
            // If the user does not have a profile photo.
            userPictureView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.smile));
        }

        gridView.setAdapter(null);

        if (standingPaymentArrayList == null || standingPaymentArrayList.size() == 0) {
            standingPaymentArrayList = calculatePayments();
        }

        ArrayList<Payment> viewList = new ArrayList<>();

        for (Payment p : standingPaymentArrayList) {
            if (p.getUser().equals(p.getReceiver())) {
                // do nothing
            } else if (p.getReceiver().equals(user.getName())) {
                p.setmCreator(user.getName());
                viewList.add(p);
            } else if (p.getUser().equals(user.getName())) {
                p.setmCreator(user.getName());
                viewList.add(p);
            }
        }

        if (standingPaymentArrayList.size() > 0) {
            uspa = new UserStandingPaymentAdapter(getContext(),
                    R.layout.user_standing_payment_list_item,
                    viewList);

            gridView.setAdapter(uspa);
        } else {
            createLog("StandingPaymentArrayList size is: " + standingPaymentArrayList.size());
        }
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    private String getFileName(File directory, String fileName) {
        File[] listFiles = directory.listFiles();
        try {
            createLog("Directory file size: " + listFiles.length);
            for (File f : listFiles) {
                if (f.getName().contains(fileName)) {
                    return f.getName();
                }
            }
        } catch (NullPointerException e) {
            createLog("No files found. ");
        }
        return fileName;
    }

    private File getTempFile(Uri pictureUri) {
        File directory = Environment.getExternalStorageDirectory();
        File filePathSegment = new File(directory, pictureUri.getLastPathSegment());

        return new File(directory, getFileName(directory, filePathSegment.getName()));
    }

    private void clearData(){
        try {
            standingArrayList.clear();
            paymentArrayList.clear();
            userStandingAdapter.clear();
            uspa.clear();

            copiedStandingArrayList.clear();
            standingPaymentArrayList.clear();
        } catch (NullPointerException e) {
            createLog(e.getMessage());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Constants.STANDING_FRAGMENT_USER, currentUser);
        outState.putString(Constants.DATABASE_NAME, databaseName);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        clearData();

        super.onDestroyView();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOAD_ALL_USERS:
                return new CursorLoader(
                        getActivity(),
                        UserEntry.CONTENT_URI_USERS,
                        UserEntry.PROJECT_ALL,
                        null,
                        null,
                        null
                );
            case LOAD_ALL_PAYMENTS:
                return new CursorLoader(
                        getActivity(),
                        PaymentEntry.CONTENT_URI_PAYMENTS,
                        PaymentEntry.PROJECT_ALL,
                        null,
                        null,
                        null
                );
            default:
                createLog("Loader id not found.");
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOAD_ALL_USERS:
                loadUsers(data);
                return;
            case LOAD_ALL_PAYMENTS:
                loadPayments(data);
                return;
            default:
                createLog("Loader Id not recognized.");
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}
