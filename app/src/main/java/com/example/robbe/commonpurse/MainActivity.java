package com.example.robbe.commonpurse;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.robbe.commonpurse.Notes.AddNoteDialog;
import com.example.robbe.commonpurse.Payments.AddPaymentDialog;
import com.example.robbe.commonpurse.Fragments.FragmentContainer;
import com.example.robbe.commonpurse.Databases.ChooseDatabaseToJoin;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 100;
    private static final String LOG_TAG = "MainActivity";

    //Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings_menu:
                Intent settingActivityIntent = new Intent(this, null);
                startActivity(settingActivityIntent);
                return true;
            case R.id.log_out_menu:
                AuthUI.getInstance()
                        .signOut(MainActivity.this);
                return true;
            case R.id.action_go_to_addPayment:
                new AddPaymentDialog().show(getSupportFragmentManager(), "AddPaymentDialog");
                return true;
            case R.id.action_add_note:
                new AddNoteDialog().show(getFragmentManager(), "AddNoteDialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //Initialize the FirebaseAuth object
        mFirebaseAuth = FirebaseAuth.getInstance();

        //These actions are for the log in of the application.
        final List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build());

        mAuthStateListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null){
                // User is logged in.
                onSignedInInitialize();
            } else {
                // User is not logged in.
                startActivityForResult(
                        AuthUI.getInstance()
                                .createSignInIntentBuilder()
                                .setIsSmartLockEnabled(false)
                                .setAvailableProviders(providers)
                                .build(),
                        RC_SIGN_IN);
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    private void onSignedInInitialize(){
        SharedPreferences prefs = getSharedPreferences(Constants.SAVED_DATABASE, MODE_PRIVATE);
        SharedPreferences userPrefs = getSharedPreferences(Constants.USERS, MODE_PRIVATE);
        String database = prefs.getString(Constants.SAVED_DATABASE, null);

        if (CheckConnectivity.hasInternetConnection(this)){
            Log.d(LOG_TAG, "Internet connection has been found");
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = firebaseAuth.getCurrentUser();

            if (userPrefs.getString(Constants.CURRENT_USER_NAME, null) == null){
                Log.d(LOG_TAG, "No saved user name in the system");
                SharedPreferences.Editor editor = userPrefs.edit();
                if (currentUser != null) {
                    editor.putString(Constants.CURRENT_USER_NAME, currentUser.getDisplayName());
                    editor.putString(Constants.CURRENT_USER_ID, currentUser.getUid());
                    editor.apply();
                }
            } else {
                Log.d(LOG_TAG, "Saved user name is: " + userPrefs.getString(Constants.CURRENT_USER_NAME, null));
            }
        }

        Log.d(LOG_TAG, "Saved database is: " + database);

        if (database != null){
            Intent intent = new Intent(MainActivity.this, FragmentContainer.class);
            intent.putExtra("DatabaseName", database);
            startActivity(intent);
        } else {
            Log.d(LOG_TAG, prefs.getString(Constants.SAVED_DATABASE, "null"));
            Intent intent = new Intent(MainActivity.this, ChooseDatabaseToJoin.class);
            startActivity(intent);
        }
    }
}
