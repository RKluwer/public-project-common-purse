package com.example.robbe.commonpurse;

import android.net.Uri;
import android.provider.BaseColumns;

import java.net.PortUnreachableException;

public class Constants {

    public Constants() {
    }

    public static final String CONTENT_AUTHOROTY = "com.example.robbe.commonpurse";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHOROTY);
    public static final String URI_PAYMENT = "payment";
    public static final String URI_USER = "user";
    public static final String URI_EXCHANGE_RATE = "exchangeRate";
    public static final String URI_DATABASES = "databases";

    public static final String DATABASES = "Databases";
    public static final String SAVED_DATABASE = "SavedDatabase";
    public static final String SAVED_DATABASE_ID = "SavedDatabaseId";
    public static final String RETURN_USER = "ReturnUser";
    public static final String SEND_USER = "SendUser";
    public static final String PASSWORD = "Password";
    public static final String DATABASE_NAME = "DatabaseName";
    public static final String DATABASE_ID = "DatabaseId";
    public static final String USERS = "Users";
    public static final String USER_ARG = "User";
    public static final String DESCRIPTION = "description";
    public static final String PAYMENTS = "Payments";
    public static final String EXCHANGE_RATE = "ExchangeRate";
    public static final String PHOTO_DATABASE = "Photos";
    public static final String USER_DETAIL_VIEW = "userDetailView";
    public static final String CURRENT_USER_NAME = "currentUserName";
    public static final String CURRENT_USER_ID = "currentUserID";
    public static final String PAYMENT_FIREBASE_KEY = "paymentFirebaseKey";
    public static final String APP_NAME = "CommonPurse";

    public static final String USER_ROLE_NORMAL = "User";
    public static final String USER_ROLE_ADMIN = "Administrator";

    public static final String USER_LIST_LOG_TAG = "UserList";
    public static final String FRAGMENT_CONTAINER_LOG_TAG = "FragmentContainer";
    public static final String CHECK_CONNECTIVITY_LOG_TAG = "CheckConnectivity";
    public static final String STANDING_FRAGMENT_USER = "StandingFragmentUser";

    public static final String LEFT_SPINNER = "LeftSpinner";
    public static final String RIGHT_SPINNER = "RightSpinner";

    public static final int TRUE = 1;
    public static final int FALSE = 0;
    public static final int TO_BE_DELETED = -1;

    // For the ExchangeRateDatabaseHelper
    public static final class ExchangeRateEntry implements BaseColumns{
        public static final String TABLE_NAME = "exchangeRate";
        public static final Uri CONTENT_URI_EXCHANGE_RATE = Uri.withAppendedPath(BASE_CONTENT_URI, URI_EXCHANGE_RATE);

        public static final String COL0_ID = BaseColumns._ID;
        public static final String COL1_APP_CURRENCY = "APPCURRENCY";
        public static final String COL2_FOREIGN_CURRENCY = "FOREIGNCURRENCY";
        public static final String COL3_EXCHANGE_RATE = "EXCHANGERATE";
        public static final String COL4_TIME_CREATED = "TIMECREATED";

        public static final String[] PROJECT_ALL = {
                COL0_ID,
                COL1_APP_CURRENCY,
                COL2_FOREIGN_CURRENCY,
                COL3_EXCHANGE_RATE,
                COL4_TIME_CREATED
        };
    }

    // For the PaymentDatabaseHelper
    public static final class PaymentEntry implements BaseColumns {

        public static final String TABLE_NAME = "payment";
        public static final Uri CONTENT_URI_PAYMENTS = Uri.withAppendedPath(BASE_CONTENT_URI, URI_PAYMENT);

        public static final String Col0_ID = BaseColumns._ID;
        public static final String COL1_FIREBASEID = "FIREBASEID";
        public static final String COL2_CURRENCY = "CURRENCY";
        public static final String COL3_USER = "USER";
        public static final String COL4_CREATOR = "CREATOR";
        public static final String COL5_CREATOR_ROLE = "CREATORROLE";
        public static final String COL6_RECEIVER = "RECEIVER";
        public static final String COL7_AMOUNT_IN_CZK = "AMOUNTINCZK";
        public static final String COL8_TOTAL_AMOUNT = "TOTALAMOUNT";
        public static final String COL9_ORIGINAL_AMOUNT = "ORIGINALAMOUNT";
        public static final String COL10_DESCRIPTION = "DESCRIPTION";
        public static final String COL11_TIME = "TIME";
        public static final String COL12_PHOTO_ID = "PHOTOID";
        public static final String COL13_APPROVED = "APPROVED";
        public static final String COL14_BASEPAYMENT = "BASEPAYMENT";
        public static final String COL15_ISPOSTED = "ISPOSTED";

        public static final String[] PROJECT_ALL = {
            Col0_ID,
                COL1_FIREBASEID,
                COL2_CURRENCY,
                COL3_USER,
                COL4_CREATOR,
                COL5_CREATOR_ROLE,
                COL6_RECEIVER,
                COL7_AMOUNT_IN_CZK,
                COL8_TOTAL_AMOUNT,
                COL9_ORIGINAL_AMOUNT,
                COL10_DESCRIPTION,
                COL11_TIME,
                COL12_PHOTO_ID,
                COL13_APPROVED,
                COL14_BASEPAYMENT,
                COL15_ISPOSTED
        };
    }

    public static final class UserEntry implements BaseColumns {

        public static final String TABLE_NAME = "user";
        public static final Uri CONTENT_URI_USERS = Uri.withAppendedPath(BASE_CONTENT_URI, URI_USER);

        public static final String Col0_id = BaseColumns._ID;
        public static final String COL1_FIREBASEID = "FirebaseID";
        public static final String COL2_FIRSTNAME = "FIRSTNAME";
        public static final String COL3_LASTNAME = "LASTNAME";
        public static final String COL4_FULLNAME = "FULLNAME";
        public static final String COL5_USERID = "USERID";
        public static final String COL6_EMAIL = "EMAILADDRESS";
        public static final String COL7_USERROLE = "USERROLE";
        public static final String COL8_TIMECREATED = "TIMECREATED";
        public static final String COL9_PHOTOURI = "PHOTOURI";

        public static final String[] PROJECT_ALL = {
                Col0_id,
                COL1_FIREBASEID,
                COL2_FIRSTNAME,
                COL3_LASTNAME,
                COL4_FULLNAME,
                COL5_USERID,
                COL6_EMAIL,
                COL7_USERROLE,
                COL8_TIMECREATED,
                COL9_PHOTOURI
        };
     }

     public static final class DatabaseEntry implements BaseColumns{
         public static final String TABLE_NAME = "databases";
         public static final Uri CONTENT_URI_DATABASES = Uri.withAppendedPath(BASE_CONTENT_URI, URI_DATABASES);

         public static final String COL0_ID = BaseColumns._ID;
         public static final String COL1_FIREBASEID = "FIREBASEID";
         public static final String COL2_TIMECREATED = "TIMECREATED";
         public static final String COL3_DATABASENAME = "DATABASENAME";
         public static final String COL4_LASTACCESSED = "LASTACCESSED";
         public static final String COL5_PASSWORD = "PASSWORD";
         public static final String COL6_CURRENCY = "CURRENCY";

         public static final String[] PROJECT_ALL = {
                 COL0_ID,
                 COL1_FIREBASEID,
                 COL2_TIMECREATED,
                 COL3_DATABASENAME,
                 COL4_LASTACCESSED,
                 COL5_PASSWORD,
                 COL6_CURRENCY
         };
     }
}
