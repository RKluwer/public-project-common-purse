package com.example.robbe.commonpurse.Users;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.robbe.commonpurse.CheckConnectivity;
import com.example.robbe.commonpurse.Constants;
import com.example.robbe.commonpurse.R;
import com.example.robbe.commonpurse.SQLite.CursorToArrayConverter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.example.robbe.commonpurse.Constants.UserEntry;

public class UserList extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOAD_ALL_USERS = 400;

    private String databaseName;
    private int requestCode = 1;

    private DatabaseReference userDatabaseReference;

    UserAdapter adapter;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.requestCode && resultCode == RESULT_OK) {
            User returnedUser = (User) data.getSerializableExtra(Constants.RETURN_USER);
            createLog(returnedUser.toString());

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            userDatabaseReference = firebaseDatabase.getReference().child(databaseName).child(Constants.USERS);

            if (returnedUser.getPhotoUri() != null) {
                Uri returnedUserUri = Uri.parse(returnedUser.getPhotoUri());
                FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
                StorageReference storageReference = firebaseStorage.getReference().child(databaseName).child(Constants.PHOTO_DATABASE);
                storageReference.child(System.currentTimeMillis() + returnedUserUri.getLastPathSegment())
                        .putFile(returnedUserUri)
                        .addOnSuccessListener(this, taskSnapshot -> {
                            String photoUrl = taskSnapshot.getDownloadUrl().toString();
                            returnedUser.setPhotoId(photoUrl);
                            returnedUser.setPhotoUri(null);

                            if (returnedUser.getFirebaseKey() != null) {
                                updateUser(returnedUser);
                            } else {
                                createUser(returnedUser);
                            }
                        });
            } else {
                if (returnedUser.getFirebaseKey() != null) {
                    updateUser(returnedUser);
                } else {
                    createUser(returnedUser);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        if (savedInstanceState != null) {
            databaseName = savedInstanceState.getString(Constants.DATABASE_NAME);
            createLog("Saved instance State is not null, database name is: " + databaseName);
        } else {
            SharedPreferences prefs = getSharedPreferences(Constants.SAVED_DATABASE, MODE_PRIVATE);
            databaseName = prefs.getString(Constants.SAVED_DATABASE, null);
            createLog("Saved instance state is null, database name is: " + databaseName);
        }

        Intent intent = getIntent();

        try {
            if (databaseName == null) {
                databaseName = intent.getStringExtra(Constants.DATABASE_NAME);
            }
            //setViewUserDetails = intent.getBooleanExtra(Constants.USER_DETAIL_VIEW, false);
            createLog("User List stared. Database: " + databaseName);
        } catch (NullPointerException e) {
            createLog(e.getMessage());
            Toast.makeText(this, "No database found, something might have gone wrong.", Toast.LENGTH_SHORT).show();
        }

        getSupportLoaderManager().restartLoader(LOAD_ALL_USERS, null, UserList.this);

        FloatingActionButton fab = findViewById(R.id.user_list_FAB);
        ListView userListView = findViewById(R.id.user_list_list_view);
        adapter = new UserAdapter(this, null);
        userListView.setAdapter(adapter);

        fab.setOnClickListener((View v) -> {
            if (CheckConnectivity.hasInternetConnection(this)) {
                Intent newUserIntent = new Intent(this, AddUserActivity.class);
                startActivityForResult(newUserIntent, requestCode);
            } else {
                Toast.makeText(this, "You can only add a new user " +
                        "when you are connected to the internet", Toast.LENGTH_SHORT).show();
            }
        });

        userListView.setOnItemClickListener((parent, view, position, id) -> {
            createLog(parent.getItemAtPosition(position).toString());
            Cursor c = (Cursor) parent.getItemAtPosition(position);
            User currentUser = CursorToArrayConverter.getUserFromCursor(c);
            createLog("Selected user is: " + currentUser.toString());

            if (CheckConnectivity.hasInternetConnection(this)) {
                Intent intentChangeUser = new Intent(this, AddUserActivity.class);
                intentChangeUser.putExtra(Constants.SEND_USER, currentUser);
                startActivityForResult(intentChangeUser, requestCode);
            } else {
                Toast.makeText(this, "You can only change a user " +
                        "when you are connected to the internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUser(User returnedUser) {
        String key = returnedUser.getFirebaseKey();
        returnedUser.setFirebaseKey(null);

        userDatabaseReference.child(key).setValue(returnedUser);
    }

    private void createUser(User user) {
        userDatabaseReference.push().setValue(user);
    }

    private void createLog(String message) {
        Log.d(Constants.USER_LIST_LOG_TAG, message);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.DATABASE_NAME, databaseName);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOAD_ALL_USERS:
                return new CursorLoader(
                        this,
                        UserEntry.CONTENT_URI_USERS,
                        UserEntry.PROJECT_ALL,
                        null,
                        null,
                        null
                );
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOAD_ALL_USERS:
                adapter.swapCursor(data);
                return;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LOAD_ALL_USERS:
                adapter.swapCursor(null);
        }
    }
}
