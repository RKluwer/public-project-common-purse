package com.example.robbe.commonpurse.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.robbe.commonpurse.Constants.PaymentEntry;

public class PaymentDatabaseHelper extends SQLiteOpenHelper {

    private String tableName;

    public PaymentDatabaseHelper(Context context, String name) {
        super(context, name, null, 2);
        createLog("Payment Database Helper has been called");
        tableName = name;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createLog("Payment Database helper, On create has been called");
        String createTable = "CREATE TABLE " + tableName + " ("
                + PaymentEntry.Col0_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PaymentEntry.COL1_FIREBASEID + " TEXT NOT NULL, "
                + PaymentEntry.COL2_CURRENCY + " TEXT NOT NULL, "
                + PaymentEntry.COL3_USER + " TEXT NOT NULL, "
                + PaymentEntry.COL4_CREATOR + " TEXT NOT NULL, "
                + PaymentEntry.COL5_CREATOR_ROLE + " TEXT, "
                + PaymentEntry.COL6_RECEIVER + " TEXT NOT NULL, "
                + PaymentEntry.COL7_AMOUNT_IN_CZK + " REAL NOT NULL, "
                + PaymentEntry.COL8_TOTAL_AMOUNT + " REAL NOT NULL, "
                + PaymentEntry.COL9_ORIGINAL_AMOUNT + " REAL, "
                + PaymentEntry.COL10_DESCRIPTION + " TEXT NOT NULL, "
                + PaymentEntry.COL11_TIME + " INTEGER NOT NULL, "
                + PaymentEntry.COL12_PHOTO_ID + " TEXT, "
                + PaymentEntry.COL13_APPROVED + " INTEGER, "
                + PaymentEntry.COL14_BASEPAYMENT + " INTEGER, "
                + PaymentEntry.COL15_ISPOSTED + " INTEGER)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + tableName);
        onCreate(db);
    }

    private void createLog (String message){
        Log.d("PaymentDatabaseHelper", message);
    }
}
