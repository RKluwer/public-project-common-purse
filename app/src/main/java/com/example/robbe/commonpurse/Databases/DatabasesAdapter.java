package com.example.robbe.commonpurse.Databases;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

public class DatabasesAdapter extends ArrayAdapter<Databases> {
    public DatabasesAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Databases> list) {
        super(context, resource, list);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
        }

        final Databases currentDb = getItem(position);
        TextView tv = listItemView.findViewById(android.R.id.text1);
        tv.setText(currentDb.getName());


        return listItemView;
    }
}
